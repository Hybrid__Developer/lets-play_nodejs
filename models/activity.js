'use strict'
const mongoose = require('mongoose')
const activitySchema = mongoose.Schema({
  
  programId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'program'
  },
  isOpenForBooking:{ type: Boolean, default: false },
  joiningLink: { type: String, default: '' },
  date: { type: Date, default: '' },
  isActivityRecurring: { type: Boolean, default: false },
  days: {
    sunday: { type: Boolean, default: false },
    monday: { type: Boolean, default: false },
    tuesday: { type: Boolean, default: false },
    wednesday: { type: Boolean, default: false },
    thursday: { type: Boolean, default: false },
    friday: { type: Boolean, default: false },
    saturday: { type: Boolean, default: false },
  },
  extractionDate: { type: Date, default: '' },
  time: {
    from: { type: Number, default: 0 },
    to: { type: Number, default: 0 },
  },
  totalSessionClasses: { type: Number, default: 0 },
  duration: {
    hours: { type: Number, default: '' },
    minutes: { type: Number, default: '' },
  },
  registrationStartDate: { type: Date, default: '' },
  registrationEndDate: { type: Date, default: '' },
  price: { type: Number, default: 0 },
  priceUnit: { type: String, default: '' },
  isPriceNotMention: { type: Boolean, default: false },
  isPriceProrated: { type: Boolean, default: false },
  offerDiscount: { type: String, default: "" },
  isFreeTrial: { type: Boolean, default: false },
  specialInstructions: { type: String, default: '' },
  cancelBeforeDays: { type: Number, default: '' },
  cancelBeforeHours: { type: Number, default: '' },
  extractor_notes: { type: String, default: '' },
  proofreaderLastReview: { type: Date, default: '' },
})

mongoose.model('activity', activitySchema)
module.exports = activitySchema
