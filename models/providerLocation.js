"use strict";
const mongoose = require("mongoose");
const providerlocation = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    name: { type: String, default: '' },
    location: { type: Object, default:{type:"Point",coordinates: [] }},
    category: { type: String, default: '' },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now }
});

mongoose.model("providerlocation", providerlocation);
module.exports = providerlocation;