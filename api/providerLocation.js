"use strict";
const service = require("../services/providerLocation");
const response = require("../exchange/response");

const create = async (req, res) => {
    const log = req.context.logger.start(`api:providerLocation:create`);
    try {
        const providerLocation = await service.create(req.body, req.context);
        log.end();
        return response.data(res, providerLocation);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const list = async (req, res) => {
    const log = req.context.logger.start(`api:providerLocation:list`);
    try {
        const providerLocations = await service.getAllproviderlocations(req.context);
        log.end();
        return response.data(res, providerLocations);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const listByUserId = async (req, res) => {
    const log = req.context.logger.start(`api:providerLocations:listByUserId`);
    try {
        const providerLocations = await service.getproviderlocationsByUserId(req.params.id, req.context);
        log.end();
        return response.data(res, providerLocations);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
const update = async (req, res) => {
    const log = req.context.logger.start(`api:providerLocations:update:${req.params.id}`);
    try {
        const providerLocations = await service.update(req.params.id,req.body, req.context);
        log.end();
        return response.data(res, providerLocations);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
exports.create = create;
exports.list = list;
exports.listByUserId = listByUserId;
exports.update=update;