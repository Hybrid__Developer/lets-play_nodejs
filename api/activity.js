"use strict";
const service = require("../services/activity");
const response = require("../exchange/response");


const create = async (req, res) => {
    const log = req.context.logger.start(`api:activity:create`);
    console.log('activity api ============>>>>>>>>>>>>>>>>>>>')
    try {
        const activity = await service.create(req.body, req.context);
        log.end();
        return response.data(res, activity);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const list = async (req, res) => {
    const log = req.context.logger.start(`api:activity:list`);
    try {
        const activity = await service.getAllActivities(req.context);
        log.end();
        return response.data(res, activity);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async (req, res) => {
    const log = req.context.logger.start(`api:activity:update`);
    try {
        const activity = await service.update(req.body, req.context);
        log.end();
        return response.data(res, activity);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const deleteActivity = async (req, res) => {
    const log = req.context.logger.start(`api:activity:deleteActivity:${req.params.id}`);
    try {
        const activity = await service.deleteActivity(req.params.id, req.context);
        log.end();
        return response.data(res, activity);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.create = create;
exports.list = list;
exports.update = update;
exports.deleteActivity = deleteActivity;
