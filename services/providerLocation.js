"use strict";

const ObjectId = require("mongodb").ObjectID;
const build = async (model, context) => {
    const { user, name,lat,lng,category } = model;
    const log = context.logger.start(`services:providerlocations:build${model}`);
    const providerlocation = await new db.providerlocation({
        user: user,
        name: name,
        location: {type:"Point",coordinates:[lng,lat]},
        category:category,
        createdOn: new Date(),
        updateOn: new Date(),
    }).save();
    log.end();
    return providerlocation;
};


const create = async (model, context) => {
    const log = context.logger.start("services:providerlocations:create");
        const providerlocation = build(model, context);
        log.end();
        return providerlocation;
    

};

const getAllproviderlocations = async (context) => {
    const log = context.logger.start(`services:providerlocations:getAllproviderlocations`);
    const providerlocations = await db.providerlocation.find({}).populate('user');
    log.end();
    return providerlocations;
};

const getproviderlocationsByUserId = async (id, context) => {
    const log = context.logger.start(`services:providerlocations:getproviderlocationsByProviderId`);
    if (!id) {
        throw new Error("userId not found");
    }
    // const providerlocations = await db.providerlocation.find({ user: query.parentId }).populate('program');
    // if (providerlocations.length < 0) {
    //     throw new Error("providerlocations not found");
    // }
    const providerlocations = await db.providerlocation.aggregate([
        {
            $match: {
                user: ObjectId(id),
            },
        },
        {
            $lookup: {
                from: 'users',
                localField: 'user',
                foreignField: '_id',
                as: 'user',
            },
        },
        {
            $unwind:'$user'
        }
    ])
    log.end();
    return providerlocations;
};

const update  = async (id,body, context) => {
    const log = context.logger.start(`services:providerlocations:update:${id}`);
    let providerlocation = await db.providerlocation.findById(id);
  if(!providerlocation){
    throw new Error('Location not find')
  }
    if(body.category !==''&&body.category!==undefined){
        providerlocation.category=body.category
    }
    if(body.name !==''&&body.name!==undefined){
        providerlocation.name=body.name
    }
    if(body.lat !==''&&body.lat!==undefined && body.lat!==0){
        providerlocation.lat=body.lat
    }
    if(body.lng !==''&&body.lng!==undefined && body.lng!==0){
        providerlocation.lng=body.lng
    }
    await providerlocation.save()
    log.end()
    return providerlocation
}



exports.create = create;
exports.getAllproviderlocations = getAllproviderlocations;
exports.getproviderlocationsByUserId = getproviderlocationsByUserId;
exports.update=update
