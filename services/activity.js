"use strict";
const build = async (model, context) => {
    const log = context.logger.start(`services:activity:build${model}`);
    console.log('activity create');
    const activity = await new db.activity({
        programId: model.programId,
        isOpenForBooking: model.isOpenForBooking,
        joiningLink: model.joiningLink,
        date: model.date,
        isActivityRecurring: model.isActivityRecurring,
        days: model.days,
        extractionDate: model.extractionDate,
        time: model.time,
        totalSessionClasses: model.totalSessionClasses,
        duration: model.duration,
        registrationStartDate: model.registrationStartDate,
        registrationEndDate: model.registrationEndDate,
        price: model.price,
        priceUnit: model.priceUnit,
        isPriceNotMention: model.isPriceNotMention,
        isPriceProrated: model.isPriceProrated,
        offerDiscount: model.offerDiscount,
        isFreeTrial: model.isFreeTrial,
        specialInstructions: model.specialInstructions,
        cancelBeforeDays: model.cancelBeforeDays,
        cancelBeforeHours: model.cancelBeforeHours,
        extractor_notes: model.extractor_notes,
        proofreaderLastReview: model.proofreaderLastReview,
        createdOn: new Date(),
        updateOn: new Date(),
    }).save();
    log.end();
    return activity;
};

const setActivity = async (model, activity, context) => {
    const log = context.logger.start("services:activity:set");
    if (model.email !== "string" && model.email !== undefined) {
        activity.email = model.email;
    }
    if (model.fromDate !== "string" && model.fromDate !== undefined) {
        activity.fromDate = model.fromDate;
    }
    if (model.toDate !== "string" && model.toDate !== undefined) {
        activity.toDate = model.toDate;
    }
    if (model.msg !== "string" && model.msg !== undefined) {
        activity.msg = model.msg;
    }
    if (model.msgType !== "string" && model.msgType !== undefined) {
        activity.msgType = model.msgType;
    }
    if (model.role !== "string" && model.role !== undefined) {
        activity.role = model.role;
    }
    if (model.activityFor !== "string" && model.activityFor !== undefined) {
        activity.activityFor = model.activityFor;
    }

    log.end();
    await activity.save();
    return activity;
}

const create = async (model, context) => {
    const log = context.logger.start("services:activity:create");
    // const isactivityExist = await db.activity.findOne({ msg: { $eq: model.msg } });
    // if (isactivityExist) {
    //     throw new Error("activity is already exist");
    // }

    const activity = build(model, context);
    log.end();
    return activity;
};

const getAllActivities = async (context) => {
    const log = context.logger.start(`services:activity:getAllActivities`);
    const activitys = await db.activity.find();
    log.end();
    return activitys;
};

const update = async (model, context) => {
    const log = context.logger.start(`services:users:update`);
    if (!model.activityId) {
        throw new Error("activity id is required");
    }

    let entity = await db.activity.findOne({ _id: model.activityId });
    if (!entity) {
        throw new Error("invalid activity");
    }
    const activity = await setActivity(model, entity, context);
    log.end();
    return activity
};

const deleteActivity = async (id, context) => {
    const log = context.logger.start(`services:users:deleteActivity:${id}`);
    if (!id) {
        throw new Error("activity id is required");
    }
    await db.activity.deleteOne({ _id: id });
    log.end();
    return 'activity Deleted Successfully'
};

exports.create = create;
exports.getAllActivities = getAllActivities;
exports.update = update;
exports.deleteActivity = deleteActivity;
