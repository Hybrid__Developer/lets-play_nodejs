'use strict'
const ObjectId = require('mongodb').ObjectID
var moment = require('moment') // require for date formating
const csv = require('csvtojson')
const fs = require('fs')
const baseUrl = require('config').get('image').baseUrl
var xlsxtojson = require("xlsx-to-json");
const _ = require('lodash');
const e = require('express')
var cron = require('node-cron');

cron.schedule('35 9 * * *', async () => {

  console.log("hi", await db.program.find({}).count())
  let update = await db.program.update(
    { 'date.to': { $lt: new Date() }, isDateNotMention: false, isExpired: false },
    { $set: { isPublished: false, isExpired: true } }, { multi: true }
  )
  console.log("update", update)
});

function humanize(str) {
  var i, frags = str.split(' ');
  for (i = 0; i < frags.length; i++) {
    frags[i] = frags[i].charAt(0).toLowerCase() + frags[i].slice(1);
  }
  return frags.join('_');
}





const buildImportProgram = async (model, context) => {
  const log = context.logger.start(
    `services:programs:buildImportProgram${model}`
  )
  let age = {
    from: 0,
    to: 0,
  }

  if (
    model.AgeRange !== '' &&
    model.AgeRange !== '-' &&
    model.AgeRange !== undefined &&
    model.AgeRange !== null
  ) {
    if (
      model.AgeRange.includes('above') ||
      model.AgeRange.includes('under') ||
      model.AgeRange.includes('all') ||
      model.AgeRange.includes('+')
    ) {
      if (model.AgeRange.includes('above')) {
        let ageRange = model.AgeRange.match(/\d+/g).map(Number)
        if (!ageRange) {
          age.from = 0
          age.to = 0
        } else {
          age.from = ageRange[0]
          age.to = 100
        }
      }

      if (model.AgeRange.includes('under')) {
        let ageRange = model.AgeRange.match(/\d+/g).map(Number)
        if (!ageRange) {
          age.from = 0
          age.to = 0
        } else {
          age.from = ageRange[0]
          age.to = 0
        }
      }
      if (model.AgeRange.includes('all')) {
        age.from = 0
        age.to = 100
      }
      if (model.AgeRange.includes('+')) {
        let ageRange = model.AgeRange.match(/\d+/g).map(Number)
        if (!ageRange) {
          age.from = 0
          age.to = 0
        } else {
          age.from = ageRange[0]
          age.to = 100
        }
      }
    } else {
      let isMonths = model.AgeRange.includes('months')
      if (isMonths) {
        age.from = 0
        age.to = 0
      } else {
        // set age range
        let ageRange = model.AgeRange.match(/\d+/g).map(Number)
        if (ageRange.length == 2) {
          age.from = ageRange[0]
          age.to = ageRange[1]
        }
      }
    }
  }

  let category
  let tag
  let categoryId
  let categories = []
  let tagByCategoryId
  let tags = []
  let date = {
    from: '',
    to: '',
  }
  let time = {
    from: '',
    to: '',
  }
  console.log('StartDateIsValid', moment(model.StartDate).isValid())
  if (
    model.StartDate !== '' &&
    model.StartDate !== '-' &&
    model.StartDate !== undefined &&
    model.StartDate !== null &&
    moment(model.StartDate).isValid()
  ) {
    date.from = moment(model.StartDate)
    log.info('date.from', date.from)
    console.log('date.from', date.from)
  } else {
    date.from = new Date()
  }

  console.log('EndDateIsValid', moment(model.StartDate).isValid())
  if (
    model.EndDate !== '' &&
    model.EndDate !== '-' &&
    model.EndDate !== undefined &&
    model.EndDate !== null &&
    moment(model.EndDate).isValid()
  ) {
    date.to = moment(model.EndDate)
    log.info('date.to', date.to)
    console.log('date.to', date.to)
  } else {
    time.to = new Date()
  }

  console.log(
    'timeFromIsValid',
    moment(model.StartDate + ' ' + model.StarTime).isValid()
  )

  if (
    model.StarTime !== '' &&
    model.StarTime !== '-' &&
    model.StarTime !== undefined &&
    model.StarTime !== null &&
    moment(model.StartDate + ' ' + model.StarTime).isValid()
  ) {
    time.from = moment(model.StartDate + ' ' + model.StarTime)
    log.info('time.from', time.from)
    console.log('time.from', time.from)
  } else {
    time.from = new Date()
  }

  console.log(
    'timeTOIsValid',
    moment(model.EndDate + ' ' + model.EndTime).isValid()
  )

  if (
    model.EndTime !== '' &&
    model.EndTime !== '-' &&
    model.EndTime !== undefined &&
    model.EndTime !== null &&
    moment(model.EndDate + ' ' + model.EndTime).isValid()
  ) {
    time.to = moment(model.EndDate + ' ' + model.EndTime)
    log.info('time.to', time.to)
    console.log('time.to', time.to)
  } else {
    time.to = new Date()
  }

  category = await db.category.findOne({ name: { $eq: model.Catogory } })

  if (!category) {
    category = await new db.category({
      name: model.Catogory ? model.Catogory : 'undefined',
      description: 'csv file data',
      createdOn: new Date(),
      updateOn: new Date(),
    }).save()
    categoryId = category.id
    await categories.push(categoryId)
  } else {
    categoryId = category.id
    console.log('category.id', category.id)
    await categories.push(categoryId)
    tagByCategoryId = await db.tag.findOne(
      { categoryIds: ObjectId(categoryId) },
      { name: model.SubCategory }
    )
  }

  if (!tagByCategoryId) {
    tag = await new db.tag({
      name: model.SubCategory ? model.SubCategory : 'undefined',
      description: 'csv file data',
      categoryIds: categories,
      createdOn: new Date(),
      updateOn: new Date(),
    }).save()
    console.log('tagID', tag.id)
    tags.push(tag.id)
  } else {
    console.log('tagID', tagByCategoryId.id)
    tags.push(tagByCategoryId.id)
  }

  let user = await db.user.findOne({
    firstName: { $regex: '.*' + model.Reference + '.*', $options: 'i' },
  })

  if (!user) {
    user = await db.user.findOne({ firstName: model.Reference })
  }

  await new db.program({
    name: model.ProgramName,
    description: model.Description,
    price: model.Price,
    ageGroup: age,
    date: date,
    time: time,
    status: model.status || 'active',
    user: user.id,
    tags: tags,
    createdOn: new Date(),
    updateOn: new Date(),
  }).save()
  log.end()
  return
}

const build = async (model, context) => {
  const log = context.logger.start(`services:programs:build${model}`)
  console.log("user", context.user)
  let word
  if (model.name) {
    word = humanize(model.name);
  }
  let isPublished = false
  if (
    model.name != '' &&
    model.name != 'string' &&
    model.type != '' &&
    model.type != 'string' &&
    model.description != '' &&
    model.description != 'string' &&
    model.date.from != '' &&
    model.date.from != 'string' &&
    model.location != '' &&
    model.location != 'string' &&
    model.ageGroup.from != '' &&
    model.ageGroup.from != 'string'
  ) {
    isPublished = true
  }
  // totalPrograms
  // categoryId array
  let count = await db.program.find({ categoryId: model.categoryId }).count();
  await db.category.findByIdAndUpdate(model.categoryId, {
    $set: {
      totalPrograms: count += 1,
    }
  })

  if (model.cycle_time !== 'string' && model.cycle_time !== undefined && model.last_reviewed !== 'DD-MM-YYYY' && model.last_reviewed !== undefined) {
    model.last_reviewed = moment(model.last_reviewed, 'DD-MM-YYYY')
    model.next_reviewed = moment(model.last_reviewed, "DD-MM-YYYY").add(model.cycle_time, 'days')._d;
  }
  else {
    model.last_reviewed = moment('25-03-1970', 'DD-MM-YYYY')
    model.next_reviewed = ''
  }
  const program = await new db.program({
    name: model.name,
    description: model.description,
    providerName: model.providerName,
    indoorOroutdoor: model.indoorOroutdoor,
    inpersonOrVirtual: model.inpersonOrVirtual,
    source: model.source,
    sourceUrl: model.sourceUrl,
    city: model.city,
    cycle: model.cycle,
    activeStatus: model.activeStatus,
    alias: word ? word : '',
    isChildCare: model.isChildCare,
    days: model.days,
    type: model.type,
    price: model.price,
    pricePeriod: model.pricePeriod,
    code: model.code,
    lat: model.lat,
    lng: model.lng,
    programCoverPic: model.programCoverPic,
    location: model.location,
    ageGroup: model.ageGroup,
    date: model.date,
    isDateNotMention: model.isDateNotMention,
    isPriceNotMention: model.isPriceNotMention,
    time: model.time,
    realTime: model.realTime,
    isTimeNotMention: model.isTimeNotMention,
    bookingCancelledIn: model.bookingCancelledIn,
    duration: model.duration,
    isFree: model.isFree,
    pricePerParticipant: model.pricePerParticipant,
    priceForSiblings: model.priceForSiblings,
    specialInstructions: model.specialInstructions,
    adultAssistanceIsRequried: model.adultAssistanceIsRequried,
    capacity: model.capacity,
    joiningLink: model.joiningLink,
    presenter: model.presenter,
    emails: model.emails,
    batches: model.batches,
    programImage: model.programImage,
    isPublished,
    status: model.status || 'active',
    user: model.userId || model.user,
    addresses: model.addresses,
    categoryId: model.categoryId,
    subCategoryIds: model.subCategoryIds,
    sessions: model.sessions,
    extractionDate: model.extractionDate,
    extractor_notes: model.extractor_notes,
    proofreaderObservation: model.proofreaderObservation,
    extractionComment: model.extractionComment,
    cyrilComment: model.cyrilComment,
    cyrilApproval: model.cyrilApproval,
    proofreaderRating: model.proofreaderRating,
    programRating: model.userRating,
    isproRated: model.isproRated,
    per_hour_rate: model.per_hour_rate,
    last_reviewed: model.last_reviewed,
    cycle_time: model.cycle_time,
    proof_reader_notes: model.proof_reader_notes,
    addedBy: context.user.id,
    addedByCreatedOn: context.user.createdOn,
    isParentJoin: model.isParentJoin,
    privateOrGroup: model.privateOrGroup,
    maxTravelDistance: model.maxTravelDistance,
    instructor: model.instructor,
    totalSessionClasses: model.totalSessionClasses,
    offerDiscount: model.offerDiscount,
    isParentGuardianRequire: model.isParentGuardianRequire,
    isOpenForBooking: model.isOpenForBooking,
    phoneNumber: model.phoneNumber,
    zip: model.zip,
    next_reviewed: model.next_reviewed,
    last_reviewed: model.last_reviewed,
    createdOn: new Date(),
    updateOn: new Date(),
  }).save()
  if (model.userId) {
    const notification = await new db.notification({
      title: 'creating program',
      description: 'Congratulations! your program is created successfully',
      user: model.userId,
      createdOn: new Date(),
      updateOn: new Date(),
    }).save()
  }

  log.end()
  return program
}
const buildTimelineUrl = async (files) => {
  let bannerImages = []
  let bannerUrl = ''
  await files.forEach((file) => {
    bannerUrl = imageUrl + file.filename
    bannerImages.push(bannerUrl)
  })
  return bannerImages
}
const set = async (model, program, context) => {
  const log = context.logger.start('services:programs:set')
  let isPublished = false
  if (
    model.name != '' &&
    model.name != 'string' &&
    model.type != '' &&
    model.type != 'string' &&
    model.description != '' &&
    model.description != 'string' &&
    model.date.from != '' &&
    model.date.from != 'string' &&
    model.location != '' &&
    model.location != 'string' &&
    model.ageGroup.from != '' &&
    model.ageGroup.from != 'string'
  ) {
    isPublished = true
  }
  if (model.name !== 'string' && model.name !== undefined) {
    program.name = model.name
  }
  if (model.name !== 'string' && model.name !== undefined) {
    program.alias = humanize(model.name)
  }

  if (model.providerName !== 'string' && model.providerName !== undefined) {
    program.providerName = model.providerName
  }
  if (model.indoorOroutdoor !== 'string' && model.indoorOroutdoor !== undefined) {
    program.indoorOroutdoor = model.indoorOroutdoor
  }
  if (model.inpersonOrVirtual !== 'string' && model.inpersonOrVirtual !== undefined) {
    program.inpersonOrVirtual = model.inpersonOrVirtual
  }
  if (model.source !== 'string' && model.source !== undefined) {
    program.source = model.source
  }
  if (model.sourceUrl !== 'string' && model.sourceUrl !== undefined) {
    program.sourceUrl = model.sourceUrl
  }
  if (model.city !== 'string' && model.city !== undefined) {
    program.city = model.city
  }
  if (model.cycle !== 'string' && model.cycle !== undefined) {
    program.cycle = model.cycle
  }
  if (model.activeStatus !== 'string' && model.activeStatus !== undefined) {
    program.activeStatus = model.activeStatus
  }

  if (model.description !== 'string' && model.description !== undefined) {
    program.description = model.description
  }
  if (model.type !== 'string' && model.type !== undefined) {
    program.type = model.type
  }
  if (model.price !== 'string' && model.price !== undefined) {
    program.price = model.price
  }
  if (model.isDateNotMention !== 'string' && model.isDateNotMention !== undefined) {
    program.isDateNotMention = model.isDateNotMention
  }
  if (model.isPriceNotMention !== 'string' && model.isPriceNotMention !== undefined) {
    program.isPriceNotMention = model.isPriceNotMention
  }
  if (model.isTimeNotMention !== 'string' && model.isTimeNotMention !== undefined) {
    program.isTimeNotMention = model.isTimeNotMention
  }
  if (model.pricePeriod !== 'string' && model.pricePeriod !== undefined) {
    program.pricePeriod = model.pricePeriod
  }
  if (model.joiningLink !== 'string' && model.joiningLink !== undefined) {
    program.joiningLink = model.joiningLink
  }
  if (model.presenter !== 'string' && model.presenter !== undefined) {
    program.presenter = model.presenter
  }
  if (model.lat !== 'string' && model.lat !== undefined) {
    program.lat = model.lat
  }
  if (model.lng !== 'string' && model.lng !== undefined) {
    program.lng = model.lng
  }
  if (
    model.programCoverPic !== 'string' &&
    model.programCoverPic !== undefined
  ) {
    program.programCoverPic = model.programCoverPic
  }
  if (model.code !== 'string' && model.code !== undefined) {
    program.code = model.code
  }
  if (model.location !== 'string' && model.location !== undefined) {
    program.location = model.location
  }
  if (model.programImage !== 'string' && model.programImage !== undefined) {
    program.programImage = model.programImage
  }
  if (
    model.ageGroup.from !== 'string' &&
    model.ageGroup.to !== 'string' &&
    model.ageGroup.to !== undefined &&
    model.ageGroup.from !== undefined
  ) {
    program.ageGroup = model.ageGroup
  }
  if (
    model.date.from !== 'string' &&
    model.date.to !== 'string' &&
    model.date.from !== undefined &&
    model.date.to !== undefined
  ) {
    program.date = model.date
  }
  if (
    model.time.from !== 'string' &&
    model.time.to !== 'string' &&
    model.time.to !== undefined &&
    model.time.from !== undefined
  ) {
    program.time = model.time
  }
  if (
    model.bookingCancelledIn !== 'string' &&
    model.bookingCancelledIn !== undefined
  ) {
    program.bookingCancelledIn = model.bookingCancelledIn
  }
  if (model.duration !== 'string' && model.duration !== undefined) {
    program.duration = model.duration
  }
  if (model.isFree !== 'string' && model.isFree !== undefined) {
    program.isFree = model.isFree
  }
  if (
    model.pricePerParticipant !== 'string' &&
    model.pricePerParticipant !== undefined
  ) {
    program.pricePerParticipant = model.pricePerParticipant
  }
  if (
    model.priceForSiblings !== 'string' &&
    model.priceForSiblings !== undefined
  ) {
    program.priceForSiblings = model.priceForSiblings
  }
  if (
    model.specialInstructions !== 'string' &&
    model.specialInstructions !== undefined
  ) {
    program.specialInstructions = model.specialInstructions
  }
  if (
    model.adultAssistanceIsRequried !== 'string' &&
    model.adultAssistanceIsRequried !== undefined
  ) {
    program.adultAssistanceIsRequried = model.adultAssistanceIsRequried
  }
  if (model.capacity !== 'string' && model.capacity !== undefined) {
    program.capacity = model.capacity
  }
  // if (model.emails.length > 1) {
  //     program.emails = model.emails;
  // }

  program.isPublished = isPublished
  if (model.batches.length) {
    program.batches = model.batches
  }
  if (model.addresses.length) {
    program.addresses = model.addresses
  }
  if (model.categoryId[0] !== 'string' && model.categoryId[0] !== undefined) {
    program.categoryId = model.categoryId
  }
  if (model.subCategoryIds[0] !== 'string' && model.subCategoryIds[0] !== '') {
    program.subCategoryIds = model.subCategoryIds
  }
  if (model.extractionDate !== 'string' && model.extractionDate !== undefined) {
    program.extractionDate = model.extractionDate
  }
  if (model.extractor_notes !== 'string' && model.extractor_notes !== undefined) {
    program.extractor_notes = model.extractor_notes
  }
  if (model.proofreaderObservation !== 'string' && model.proofreaderObservation !== undefined) {
    program.proofreaderObservation = model.proofreaderObservation
  }
  if (model.extractionComment !== 'string' && model.extractionComment !== undefined) {
    program.extractionComment = model.extractionComment
  }
  if (model.cyrilComment !== 'string' && model.cyrilComment !== undefined) {
    program.cyrilComment = model.cyrilComment
  }
  if (model.cyrilApproval !== 'string' && model.cyrilApproval !== undefined) {
    program.cyrilApproval = model.cyrilApproval
  }
  if (model.proofreaderRating !== 'string' && model.proofreaderRating !== undefined) {
    program.proofreaderRating = model.proofreaderRating
  }

  if (model.sessions.length) {
    program.sessions = model.sessions
  }
  if (model.days) {
    program.days = model.days
  }
  if (model.per_hour_rate !== 'string' && model.per_hour_rate !== undefined) {
    program.per_hour_rate = model.per_hour_rate
  }
  if (model.last_reviewed !== '28-06-1970' && model.last_reviewed !== undefined) {
    program.last_reviewed = model.last_reviewed
  }
  if (model.cycle_time !== 'string' && model.cycle_time !== undefined) {
    program.cycle_time = model.cycle_time
  }
  if (model.proof_reader_notes !== 'string' && model.proof_reader_notes !== undefined) {
    program.proof_reader_notes = model.proof_reader_notes
  }
  if (model.isParentJoin !== 'string' && model.isParentJoin !== undefined) {
    program.isParentJoin = model.isParentJoin
  }
  if (model.isChildCare !== 'string' && model.isChildCare !== undefined) {
    program.isChildCare = model.isChildCare
  }
  if (model.privateOrGroup !== 'string' && model.privateOrGroup !== undefined) {
    program.privateOrGroup = model.privateOrGroup
  }
  if (model.maxTravelDistance !== 'string' && model.maxTravelDistance !== undefined) {
    program.maxTravelDistance = model.maxTravelDistance
  }
  if (model.instructor !== 'string' && model.instructor !== undefined) {
    program.instructor = model.instructor
  }
  if (model.totalSessionClasses !== 'string' && model.totalSessionClasses !== undefined) {
    program.totalSessionClasses = model.totalSessionClasses
  }
  if (model.offerDiscount !== 'string' && model.offerDiscount !== undefined) {
    program.offerDiscount = model.offerDiscount
  }
  if (model.isParentGuardianRequire !== 'string' && model.isParentGuardianRequire !== undefined) {
    program.isParentGuardianRequire = model.isParentGuardianRequire
  }
  if (model.isOpenForBooking !== 'string' && model.isOpenForBooking !== undefined) {
    program.isOpenForBooking = model.isOpenForBooking
  }
  if (model.phoneNumber !== 'string' && model.phoneNumber !== undefined) {
    program.phoneNumber = model.phoneNumber
  }
  if (model.zip !== 'string' && model.zip !== undefined) {
    program.zip = model.zip
  }
  if (model.last_reviewed !== 'DD-MM-YYYY' && model.last_reviewed !== undefined) {
    console.log("modeldate", model.last_reviewed)
    program.last_reviewed = moment(model.last_reviewed, 'DD-MM-YYYY')
  }
  if (model.cycle_time !== 'string' && model.cycle_time !== undefined && model.last_reviewed !== 'DD-MM-YYYY' && model.last_reviewed !== undefined) {
    model.last_reviewed = moment(model.last_reviewed, 'DD-MM-YYYY')
    program.next_reviewed = moment(model.last_reviewed, "DD-MM-YYYY").add(model.cycle_time, 'days')._d;
  }
  program.isproRated = model.isproRated
  program.isExpired = model.isExpired
  program.lastModifiedBy = context.user.id
  program.updatedOn = new Date()
  log.end()
  await program.save()
  return program
}

const create = async (model, context) => {
  const log = context.logger.start('services:programs:create')

  let user = await db.user.findById(model.userId)

  if (user.role == 'parent') {
    throw new Error('you are not authorized to perform this operation')
  }
  if (user.role !== 'admin') {
    if (
      !isObjKeyHasValue(user.phoneNumber) ||
      !isObjKeyHasValue(user.avatarImages) ||
      !isObjKeyHasValue(user.addressLine1)
    ) {
      throw new Error('your profile is incomplete you cannot add program!')
    }
  }
  model.userRating = user.averageFinalRating;
  const program = build(model, context)
  let programBuild = await db.program.findOne({ _id: program._id, "$expr": { "$eq": ["$date.from", "$date.to"] }, isDateNotMention: false })
  if (programBuild) {
    program.isDateNotMention = true
    await program.save()
  }
  log.end()
  return program
}

const isObjKeyHasValue = async (value) => {
  if (value == '' || value == undefined || value == 'string') {
    return false
  }
  return true
}

const getAllprograms = async (query, context) => {
  const log = context.logger.start(`services:programs:getAllprograms`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs = await db.program
    .find()
    .sort({ _id: -1 })
    .populate('tags')
    .populate('user')
    .populate('addedBy')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
    .skip(skipCount)
    .limit(pageSize)
  programs.count = await db.program.find().count()
  let favourites
  if (context.user !== undefined) {
    favourites = await db.favourite
      .find({ user: context.user.id })
      .populate('program')
  }
  if (favourites) {
    // add fav in program
    for (var p = 0; p < programs.length; p++) {
      for (var f = 0; f < favourites.length; f++) {
        if (
          favourites[f].program !== null &&
          favourites[f].program !== undefined
        ) {
          if (programs[p].id === favourites[f].program.id) {
            programs[p].isFav = true
          }
        }
      }
    }
  }
  log.end()
  return programs
}
const getByType = async (query, context) => {
  const log = context.logger.start(`services:programs:getAllprograms`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs = await db.program
    .find({ type: { $regex: '.*' + query.type + '.*', $options: 'i' } })
    .sort({ _id: -1 })
    .populate('tags')
    .populate('user')
    .populate('addedBy')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
    .skip(skipCount)
    .limit(pageSize)
  programs.count = await db.program.find({ type: { $regex: '.*' + query.type + '.*', $options: 'i' } }).count()
  programs.types = await db.program.distinct("type")
  // let favourites
  // if (context.user !== undefined) {
  //   favourites = await db.favourite
  //     .find({ user: context.user.id })
  //     .populate('program')
  // }
  // if (favourites) {
  //   // add fav in program
  //   for (var p = 0; p < programs.length; p++) {
  //     for (var f = 0; f < favourites.length; f++) {
  //       if (
  //         favourites[f].program !== null &&
  //         favourites[f].program !== undefined
  //       ) {
  //         if (programs[p].id === favourites[f].program.id) {
  //           programs[p].isFav = true
  //         }
  //       }
  //     }
  //   }
  // }
  log.end()
  return programs
}
const getById = async (id, context) => {
  const log = context.logger.start(`services:programs:getById`)
  if (!id) {
    throw new Error('id not found')
  }
  let program = await db.program
    .findById(id)
    .sort({ createdOn: -1 })
    .populate('tags')
    .populate('user')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
  if (program) {
    let favourite = await db.favourite.findOne({ program: program.id })
    if (favourite) {
      program.isFav = true
    }
  }
  log.end()
  return program
}

const update = async (id, model, context) => {
  const log = context.logger.start(`services:programs:update`)
  if (!id) {
    throw new Error('id Not found')
  }
  if (context.user.role == 'parent') {
    throw new Error('you are not authorized to perform this operation')
  }
  let programDetail = await db.program.findById(id)

  if (!programDetail) {
    throw new Error('program Not found')
  }

  const program = await set(model, programDetail, context)
  let programBuild = await db.program.findOne({ _id: program._id, "$expr": { "$eq": ["$date.from", "$date.to"] }, isDateNotMention: false })
  if (programBuild) {
    program.isDateNotMention = true
    await program.save()
  }
  log.end()
  return program
}

const removeById = async (id, context) => {
  const log = context.logger.start(`services:programs:removeById`)
  if (!id) {
    throw new Error('programs id not found')
  }
  let isDeleted = await db.program.deleteOne({ _id: id })
  if (!isDeleted) {
    throw new Error('something went wrong')
  }
  else {
    await db.favourite.deleteMany({ program: ObjectId(id) })
  }
  log.end()
  return 'program deleted succesfully'
}

const uploadTimeLinePics = async (id, files, context) => {
  const log = context.logger.start(`services:programs:uploadTimeLinePics`)
  const program = await db.program.findOne({ _id: id })

  if (files.length < 0) {
    throw new Error('image not found')
  }

  if (!program) {
    throw new Error('program not found')
  }

  let pics = await buildTimelineUrl(files)
  program.timelinePics = pics
  await program.save()
  log.end()
  return program
}

const search = async (query, context) => {
  const log = context.logger.start(`services:programs:search`)
  const program = await db.program
    .find({ name: { $regex: '.*' + query.name + '.*', $options: 'i' } })
    .populate('tags')
    .populate('subCategoryIds')
    .limit(5)
  log.end()
  let finalProgram = []
  program.forEach((progrm, index) => {
    if (
      progrm.name != '' &&
      progrm.name != 'string' &&
      progrm.type != '' &&
      progrm.type != 'string' &&
      progrm.description != '' &&
      progrm.description != 'string' &&
      progrm.date.from != '' &&
      progrm.date.from != 'string' &&
      progrm.price != '' &&
      progrm.price != 'string' &&
      progrm.location != '' &&
      progrm.location != 'string' &&
      progrm.ageGroup.from != '' &&
      progrm.ageGroup.from != 'string'
    ) {
      finalProgram.push(progrm)
    } else {
      console.log('')
    }
  })
  log.end()
  return finalProgram
}
// name, type, description, date, price, location, ageGroup,
const getProgramsByProvider = async (query, context) => {
  const log = context.logger.start(`services:programs:getProgramsByProvider`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  if (!query.userId) {
    throw new Error('userId is  required')
  }
  let user = await db.user.findById(query.userId)
  if (!user) {
    throw new Error(
      'provider not found, So without provider programs not possible'
    )
  }
  // let programs = await db.program.find({ user: query.userId }).sort({ createdOn: -1 }).populate('tags').skip(skipCount).limit(pageSize);
  let programs = await db.program.aggregate([
    {
      $match: {
        user: ObjectId(query.userId),
        isPublished: true
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'categoryId',
        foreignField: '_id',
        as: 'categoryId',
      },
    },
    {
      $lookup: {
        from: 'providers',
        localField: 'user',
        foreignField: 'user',
        as: 'provider',
      },
    },
    {
      $lookup: {
        from: "tags",
        let: { "subCategoryIds": "$subCategoryIds" },
        // pipeline: [
        //   { "$match": { "$expr": { "$in": ["$_id", "$$subCategoryIds"] } } }
        // ],
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $in: ['$_id', { $ifNull: ['$$subCategoryIds', []] }] },
                ]
              }
            }
          }
        ],
        as: "subCategoryIds"
      }
    },
    { $limit: pageSize + skipCount },
    { $skip: skipCount },
  ])
  programs.count = await db.program.find({ user: query.userId }).count()
  log.end()
  let favourites
  if (user) {
    favourites = await db.favourite
      .find({ user: query.userId })
      .populate('program')
  }
  if (favourites) {
    // add fav in program
    for (var p = 0; p < programs.length; p++) {
      for (var f = 0; f < favourites.length; f++) {
        if (
          favourites[f].program !== null &&
          favourites[f].program !== undefined
        ) {
          if (programs[p]._id == favourites[f].program.id) {
            programs[p].isFav = true
          }
        }
      }
    }
  }
  if (query.grouping == "true") {
    var result = programs.filter(function (a) {
      var key = a.ageGroup.from + '|' + a.ageGroup.to + '|' + a.type;
      if (!this[key]) {
        this[key] = true;
        return true;
      }
    }, Object.create(null));
    programs = result
    programs.count = result.length
  }


  return programs
}
const addProgramAction = async (model, context) => {
  const log = context.logger.start('services:programs:increaseViewCount')
  let programActionCounter

  if (!model.programId) {
    throw new Error('program id is requried')
  }

  programActionCounter = await db.programActionCounter.findOne({
    $and: [{ user: context.user.id }, { program: model.programId }],
  })

  let count = 1

  if (model.action == 'view') {
    if ((programActionCounter != null) & (programActionCounter != undefined)) {
      programActionCounter.view = count += programActionCounter.view
      await programActionCounter.save()
    } else {
      programActionCounter = await new db.programActionCounter({
        view: count,
        program: model.programId,
        user: context.user.id,
        createdOn: new Date(),
        updateOn: new Date(),
      }).save()
    }
  } else if (model.action == 'click') {
    if (programActionCounter != null && programActionCounter != undefined) {
      programActionCounter.click = count += programActionCounter.click
      await programActionCounter.save()
    } else {
      programActionCounter = await new db.programActionCounter({
        click: count,
        program: model.programId,
        user: context.user.id,
        createdOn: new Date(),
        updateOn: new Date(),
      }).save()
    }
  } else if (model.action == 'favourite') {
    if (programActionCounter != null && programActionCounter != undefined) {
      programActionCounter.favourite = count += programActionCounter.favourite
      await programActionCounter.save()
    } else {
      click = await new db.programActionCounter({
        favourite: count,
        program: model.programId,
        user: context.user.id,
        createdOn: new Date(),
        updateOn: new Date(),
      }).save()
    }
  }
  log.end()
  return programActionCounter
}
const changeProgramDate = async (query, context) => {
  const log = context.logger.start('services:programs:changeProgramDate')
  const days = query.days || 1
  const operation = query.operation
  let update
  if (operation == "add") {
    const programs = await db.program.find({ isDateNotMention: false, isExpired: false })
    programs.forEach(async function (program) {

      update = await db.program.update(
        { _id: program._id }
        , { $set: { 'date.from': new Date(program.date.from.getTime() + (86400000 * days)), 'date.to': new Date(program.date.to.getTime() + (86400000 * days)) } }
      );

    });
  }
  else if (operation == "subtract") {
    const programs = await db.program.find({ isDateNotMention: false, isExpired: false })
    programs.forEach(async function (program) {

      update = await db.program.update(
        { _id: program._id }
        , { $set: { 'date.from': new Date(program.date.from.getTime() - (86400000 * days)), 'date.to': new Date(program.date.to.getTime() - (86400000 * days)) } }
      );

    });
  }
  log.end()
  console.log(update)
  return `Change date of programs is done`
}
const getViewCount = async (query, context) => {
  const log = context.logger.start(`services:programs:getViewCount`)
  if (!query.userId) {
    throw new Error('userId not found')
  }
  const viewCount = await db.programActionCounter.aggregate([
    {
      $lookup: {
        from: 'programs',
        localField: 'program',
        foreignField: '_id',
        as: 'program',
      },
    },
    {
      $match: {
        'program.user': ObjectId(query.userId),
      },
    },

    { $group: { _id: null, view: { $sum: '$view' } } },
  ])
  let data = {}
  if (viewCount.length) {
    data.count = viewCount[0].view
  } else {
    data.message = 'somethng went wrong'
  }

  log.end()
  return data
}
const getProgramCount = async (query, context) => {
  const log = context.logger.start(`services:programs:getProgramCount`)
  if (!query.userId) {
    throw new Error('userId is requried')
  }
  const count = await db.program
    .find({ $and: [{ user: query.userId }, { status: 'active' }] })
    .count()
  log.end()
  return count
}
const setActiveOrDecactive = async (query, context) => {
  const log = context.logger.start(`services:programs:getProgramCount`)
  if (!query.id) {
    throw new Error('program id is requried')
  }
  if (!query.status) {
    throw new Error('program status is requried')
  }
  let program = await db.program.findById(query.id)
  program.status = query.status
  await program.save()
  log.end()
  return program
}
const getGraphData = async (query, context) => {
  const log = context.logger.start(`services:programs:getGraphData`)
  if (!query.id) {
    throw new Error('userId is requried')
  }
  const data = await db.programActionCounter
    .aggregate([
      {
        $lookup: {
          from: 'programs',
          localField: 'program',
          foreignField: '_id',
          as: 'program',
        },
      },
      {
        $match: {
          'program.user': ObjectId(query.id),
        },
      },
      {
        $group: {
          _id: '$program.name',
          view: { $sum: '$view' },
          click: { $sum: '$click' },
          favourite: { $sum: '$favourite' },
        },
      },
      {
        $sort: {
          _id: -1,
        },
      },
    ])
    .limit(5)

  let barChartRes = {
    barChartLabels: [],
    barChartData: [
      {
        label: 'Views',
        data: [],
      },
      {
        label: 'Clicks',
        data: [],
      },
      {
        label: 'Favourites',
        data: [],
      },
    ],
  }
  try {
    data.forEach((item) => {
      barChartRes.barChartLabels.push(item._id[0])
      barChartRes.barChartData.forEach((ChartData) => {
        if (ChartData.label == 'Views') {
          ChartData.data.push(item.view)
        } else if (ChartData.label == 'Clicks') {
          ChartData.data.push(item.click)
        } else if (ChartData.label == 'Favourites') {
          ChartData.data.push(item.favourite)
        }
      })
    })
  } catch (err) {
    throw new Error('barChartData mapping failed')
  }
  log.end()
  return barChartRes
}

// const getFilterProgram = async (model, context) => {
//   const log = context.logger.start(`services:programs:getFilterProgram`);
//   let pageNo = Number(model.pageNo) || 1;
//   let pageSize = Number(model.pageSize) || 10;
//   let skipCount = pageSize * (pageNo - 1);
//   let query = {}
//   if (model.ageFrom && model.ageTo) {
//     query["ageGroup.from"] = { $gte: Number(model.ageFrom) }
//     query["ageGroup.to"] = { $lte: Number(model.ageTo) }
//   }

//   if (model.fromDate !== undefined && model.toDate !== undefined && model.fromDate !== "" && model.toDate !== "" && model.fromDate !== null && model.toDate !== null) {
//     query["date.from"] = { $gte: model.fromDate }
//     query["date.to"] = { $lte: model.toDate }
//   }

//   if (model.toTime !== undefined && model.fromTime !== undefined && model.toTime !== "" && model.fromTime !== "" && model.toTime !== null && model.fromTime !== null) {
//     query["time.from"] = { $gte: new Date(model.fromTime).getTime() }
//     query["date.to"] = { $lte: new Date(model.toTime).getTime() }
//   }


//   let programs = await db.program.find(query).populate('tags').skip(skipCount).limit(pageSize);
//   programs.count = await db.program.find({ user: query.userId }).count();

//   log.end();
//   return programs;
// };

const getFilterProgram = async (query, context) => {
  const log = context.logger.start(`services:programs:getFilterProgram`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs
  let filterQueries = query;
  // programs = await db.program
  //   .find({ createdOn: dat, isPublished: true })
  //   .populate('tags')
  //   .populate('categoryId')
  //   .populate('subCategoryIds')
  //   .populate('user')
  //   .skip(skipCount)
  //   .limit(pageSize)


  if (query.fromDate && query.toDate) {
    const dat = {
      $gte: moment(query.fromDate, 'DD-MM-YYYY').startOf('day').toDate(),
      $lt: moment(query.toDate, 'DD-MM-YYYY').endOf('day').toDate(),
    }
    programs = await db.program
      .find({ createdOn: dat, isPublished: true })
      .populate('tags')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
  }

  if (query.ageFrom && query.ageTo) {
    const age = {
      $gte: Number(query.ageFrom),
      $lte: Number(query.ageTo),
    }

    programs = await db.program
      .find({
        $or: [{ 'ageGroup.from': age, 'ageGroup.to': age, isPublished: true }],
      })
      .populate('tags')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
  }

  if (query.fromTime && query.toTime) {
    // const tme = {
    //   $gte: moment(query.fromTime, 'DD-MM-YYYY').startOf('day').toDate(),
    //   $lt: moment(query.toTime, 'DD-MM-YYYY').endOf('day').toDate(),
    // }
    const tme = {
      $gte: query.fromTime,
      $lt: query.toTime,
    }

    programs = await db.program
      .find({ "realTime.from": tme })
      .populate('tags')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
  }

  if (query.priceFrom && query.priceTo) {
    const byPrice = {
      $gte: query.priceFrom,
      $lte: query.priceTo,
    }
    programs = await db.program
      .find({ price: byPrice, isPublished: true })
      .populate('tags')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
  }

  if (query.durationMin && query.durationMax) {
    const byduration = {
      $gte: query.durationMin,
      $lte: query.durationMax,
    }
    programs = await db.program
      .find({ duration: byduration, isPublished: true })
      .populate('tags')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
  }

  if (query.categoryId) {
    programs = await db.program
      .find({ categoryId: query.categoryId, isPublished: true })
      .populate('tags')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
  }

  if (query.type1 || query.type2) {
    programs = await db.program
      .find({ $or: [{ type: query.type1 }, { type: query.type2 }], isPublished: true })
      .populate('tags')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
  }

  log.end()
  return programs
}

const importProgram = async (file, context) => {
  const log = context.logger.start('services:program:importProgram')
  if (file.fieldname != 'csv') {
    throw new Error('please provide csv file')
  }

  const rows = await csv().fromFile(file.path)
  if (rows.length < 1) {
    throw new Error('csv file is empty !please provide some data ')
  }

  let count = 0
  for (let row of rows) {
    if (
      row.ProgramName !== '' &&
      row.ProgramName !== null &&
      row.ProgramName !== undefined
    ) {
      count++
      await buildImportProgram(row, context)
    }
  }
  log.info(`${count} record inserted `)
  await fs.unlinkSync(file.path)
  return `total record inserted ${count}`
}

const getProgramsByDate = async (query, context) => {
  const { fromDate, toDate } = query
  const log = context.logger.start(`services:programs:getProgramsByDate`)
  const dat = {
    $gte: moment(fromDate, 'DD-MM-YYYY').startOf('day').toDate(),
    $lt: moment(toDate, 'DD-MM-YYYY').endOf('day').toDate(),
  }
  let programs = await db.program.find({ createdOn: dat }).populate('subCategoryIds')
  let count = await db.program.find({ createdOn: dat }).count()
  log.end()
  return programs
}

const publishedOrUnPublishedPrograms = async (query, context) => {
  const { userId, programType } = query
  const log = context.logger.start(
    `services:programs:publishedOrUnPublishedPrograms`
  )
  if (!userId) {
    throw new Error('user id is required')
  }
  let programs = await db.program.find({ user: userId }).populate('subCategoryIds')
  if (!programs) {
    throw new Error('programs not found')
  }
  let finalProgram = []
  if (programType == 'published') {
    programs.forEach((progrm, index) => {
      if (
        progrm.name != '' &&
        progrm.name != 'string' &&
        progrm.type != '' &&
        progrm.type != 'string' &&
        progrm.description != '' &&
        progrm.description != 'string' &&
        progrm.date.from != '' &&
        progrm.date.from != 'string' &&
        progrm.location != '' &&
        progrm.location != 'string' &&
        progrm.ageGroup.from != '' &&
        progrm.ageGroup.from != 'string'
      ) {
        finalProgram.push(progrm)
      } else {
        console.log('')
      }
    })
  }
  if (programType == 'unpublished') {
    programs.forEach((progrm, index) => {
      if (
        progrm.name == '' ||
        progrm.name == 'string' ||
        progrm.type == '' ||
        progrm.type == 'string' ||
        progrm.description == '' ||
        progrm.description == 'string' ||
        progrm.date.from == '' ||
        progrm.date.from == 'string' ||
        progrm.location == '' ||
        progrm.location == 'string' ||
        progrm.ageGroup.from == '' ||
        progrm.ageGroup.from == 'string'
      ) {
        finalProgram.push(progrm)
      } else {
        console.log('')
      }
    })
  }

  let count = await finalProgram.length
  let data = {
    count,
    finalProgram,
  }
  log.end()
  return data
}

const openPrograms = async (query, context) => {
  const log = context.logger.start(`services:programs:openPrograms`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs = await db.program
    .find({ isPublished: true })
    .sort({ createdOn: -1 })
    .populate('tags')
    .populate('subCategoryIds')
    .skip(skipCount)
    .limit(pageSize)
  let finalProgram = []
  let current = new Date()
  programs.forEach((progrm, index) => {
    if (moment(progrm.date.to).isSameOrAfter(current, 'day')) {
      finalProgram.push(progrm)
    }
  })
  log.end()
  finalProgram.count = finalProgram.length
  return finalProgram
}

const publish = async (query, context) => {
  const log = context.logger.start(`services:providers:publish`)

  let program = await db.program.findById(query.programId)
  if (context.user.role == 'parent') {
    throw new Error('you are not authorized to perform this operation')
  }
  // if (
  //   program.name == '' ||
  //   program.name == 'string' ||
  //   program.type == '' ||
  //   program.type == 'string' ||
  //   program.description == '' ||
  //   program.description == 'string' ||
  //   program.date.from == '' ||
  //   program.date.from == 'string' ||
  //   program.location == '' ||
  //   program.location == 'string' ||
  //   program.ageGroup.from == '' ||
  //   program.ageGroup.from == 'string'
  // ) {
  //   throw new Error('you need to complete the program before publish it')
  // }
  program.isPublished = query.isPublished
  program.updatedOn = new Date()
  log.end()
  program.save()
  return program
}


const listPublishOrUnpublish = async (query, context) => {
  const log = context.logger.start(`services:providers:listPublishOrUnpublish`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs
  if (query.programType == 'published') {
    const programs = await db.program.aggregate([
      {
        $match: {
          isPublished: true,
        },
      },
      {
        $lookup: {
          from: 'categories',
          localField: 'categoryId',
          foreignField: '_id',
          as: 'categoryId',
        },
      },
      {
        $lookup: {
          from: "tags",
          let: { "subCategoryIds": "$subCategoryIds" },
          // pipeline: [
          //   { "$match": { "$expr": { "$in": ["$_id", "$$subCategoryIds"] } } }
          // ],
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $in: ['$_id', { $ifNull: ['$$subCategoryIds', []] }] },
                  ]
                }
              }
            }
          ],
          as: "subCategoryIds"
        }
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $sort: { programRating: 1 } },
      { $limit: pageSize + skipCount },
      { $skip: skipCount },
    ])
    programs.count = await db.program.find({ isPublished: true }).count()
    log.end()
    return programs.reverse()
  }
  if (query.programType == 'unpublished') {
    programs = await db.program
      .find({ isPublished: false })
      .sort({ createdOn: -1 })
      .populate('tags')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
    programs.count = await db.program.find({ isPublished: false }).count()
  }
  log.end()
  return programs
}
const groupPublishOrUnpublish = async (query, context) => {
  const log = context.logger.start(`services:providers:groupPublishOrUnpublish`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  if (query.programType == 'published') {
    let programs = await db.program.aggregate([
      {
        $match: {
          isPublished: true,
          isExpired: false
        },
      },
      { $sort: { user: 1 } },
      {
        $group:
        {
          _id: "$user",
          total: { $sum: 1 },
          programs: { $push: "$$ROOT" },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'programs.user',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $lookup: {
          from: 'categories',
          localField: 'programs.categoryId',
          foreignField: '_id',
          as: 'categories',
        },
      },
      { $unwind: "$user" },
      {
        $match: {
          "user.isActivated": true
        }
      },
      { $sort: { "user.averageFinalRating": -1, "user.createdOn": -1 } },
      { $limit: pageSize + skipCount },
      { $skip: skipCount },
    ])
    programs.map(async (program) => {
      var result = program.programs.reverse().filter( function (a) {
        var key = a.ageGroup.from + '|' + a.ageGroup.to + '|' + a.type;
        console.log("key",key)
        if (!this[key]) {
          this[key] = true;
          return true;
        }
      }, Object.create(null));
      program.programs = result
      if (context.user !== undefined) {
        const Favourites = await db.saveProvider.findOne({ $and: [{ parent: context.user.id }, { provider: program._id }] })
        if (Favourites) {
          program.user.isFav = true
        }
      }
      program.programs.map(async(a)=>{
        var isfav=await checkFav(a,context)
        if(isfav==true){
         a.isFav=true
        }
        return a
      })
      let locations= await db.providerlocation.aggregate([
        {
            $match: {
                user: program._id,
            },
        },
    ])
    program.multiLoctions=locations
      return program.programs.sort((a, b) => new Date(b.date.from).valueOf() - new Date(a.date.from).valueOf());
    })
  
    const allprograms = await db.program.aggregate([
      {
        $match: {
          isPublished: true,
          isExpired: false
        },
      },
      {
        $group:
        {
          _id: "$user",
          total: { $sum: 1 },
          programs: { $push: "$$ROOT" },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'programs.user',
          foreignField: '_id',
          as: 'user',
        },
      },
      { $unwind: "$user" },
      {
        $match: {
          "user.isActivated": true
        }
      },
      { $count: "totalCount" }
    ])
    log.end()
    console.log("allprograms", allprograms)
    let calculationPrograms = {
      totalPrograms: await db.program.find({ isExpired: false }).count(),
      publishPrograms: await db.program.find({ isPublished: true, isExpired: false }).count(),
      unpublishPrograms: await db.program.find({ isPublished: false, isExpired: false }).count(),
      totalProviders: allprograms.length > 0 ? allprograms[0].totalCount : 0
    }

    return { programs: programs, calculationPrograms: calculationPrograms }
  }
  if (query.programType == 'unpublished') {
    let programs = await db.program
      .find({ isPublished: false })
      .sort({ createdOn: -1 })
      .populate('tags')
      .populate('user')
      .skip(skipCount)
      .limit(pageSize)
    programs.count = await db.program.find({ isPublished: false, isExpired: false }).count()
    let calculationPrograms = {
      totalPrograms: await db.program.find({ isExpired: false }).count(),
      publishPrograms: await db.program.find({ isPublished: true, isExpired: false }).count(),
      unpublishPrograms: await db.program.find({ isPublished: false, isExpired: false }).count(),
      totalProviders: allprograms[0].totalCount
    }
    programs.calculationPrograms = calculationPrograms
    log.end()
    return programs
  }

}
const  checkFav= async(a,context)=>{
  let favourites
        if (context.user !== undefined) {
          favourites = await db.favourite
            .findOne({ user: context.user.id, program: a._id })
            if (favourites) {
              console.log("tttt")
              return true
            }
            else{
              console.log("fdfdf")
              return false
            }
        }
        else{
          console.log("fdfdf")
          return false
        }
        
}
const searchByNameAndDate = async (query, context) => {
  const { programName, date } = query
  const log = context.logger.start(`services:programs:searchByNameAndDate`)

  // const d = {
  //   $gte: moment(date, 'DD-MM-YYYY').startOf('day').toDate(),
  //   $lt: moment(date, 'DD-MM-YYYY').endOf('day').toDate(),
  // }
  let programs1
  let tagPrograms = []
  let categoryPrograms = []
  if (programName) {
    programs1 = await db.program
      .find({
        name: { $regex: '.*' + programName + '.*', $options: 'i' },
        isPublished: true,
      }).populate('categoryId').populate('subCategoryIds').populate('user').limit(10)
    // console.log('programs1 =>>', programs1);
    let tag = await db.tag.find({ name: { $regex: '.*' + programName + '.*', $options: 'i' } }).limit(5)
    console.log('tag ---------', tag.length);
    if (tag.length >= 1) {
      tagPrograms = await db.program
        .find({ subCategoryIds: tag[0]._id, isPublished: true }).populate('categoryId').populate('subCategoryIds').populate('user').limit(10)
    }
    let category = await db.category.find({ name: { $regex: '.*' + programName + '.*', $options: 'i' } }).limit(5)
    if (category.length >= 1) {
      categoryPrograms = await db.program
        .find({ categoryId: category[0]._id, isPublished: true }).populate('categoryId').populate('subCategoryIds').populate('user').limit(10)
    }
  }

  let programs = programs1.concat(tagPrograms, categoryPrograms);
  programs = [...new Set([...programs1, ...tagPrograms, ...categoryPrograms])]
  // console.log('programs ==>>>', programs)

  let favourites
  if (context.user !== undefined) {
    favourites = await db.favourite
      .find({ user: context.user.id })
      .populate('program')
  }
  if (favourites) {
    // add fav in program
    for (var p = 0; p < programs.length; p++) {
      for (var f = 0; f < favourites.length; f++) {
        if (
          favourites[f].program !== null &&
          favourites[f].program !== undefined
        ) {
          if (programs[p].id === favourites[f].program.id) {
            programs[p].isFav = true
          }
        }
      }
    }
  }
  log.end()
  return programs
}

// if (date) {
//   programs = await db.program.find({ createdOn: d, isPublished: true })
//     .populate('categoryId')
//     .populate('subCategoryIds')
//     .populate('user')
// }
// if (programName && date) {
//   programs = await db.program.find({
//     name: { $regex: '.*' + programName + '.*', $options: 'i' },
//     createdOn: d,
//     isPublished: true,
//   })
//     .populate('categoryId')
//     .populate('subCategoryIds')
//     .populate('user')
// }

const topRating = async (context) => {
  const log = context.logger.start(`services:programs:topRating`)
  // const users = await db.user.find({ averageFinalRating: { $gte: 4.0, $lte: 5.0 } });
  // let totalPrograms = []
  // for (let user of users) {
  //   console.log(user.id);
  //   const programs = await db.program.find({ user: user.id })
  //     .populate('tags')
  //     .populate('categoryId')
  //     .populate('subCategoryIds')
  //     .populate('user')
  //   totalPrograms.push(...programs)
  // }
  const programs = await db.program.find({ programRating: { $gte: 4.0, $lte: 5.0 } }).sort({ programRating: -1 })
    .populate('tags')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('user')
  log.end()
  return programs
}

const multiFilter = async (model, context) => {
  const log = context.logger.start(`services:programs:multiFilter`)
  let pageNo = Number(model.pageNo) || 1
  let pageSize = Number(model.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  // if (query.type1 || query.type2) {
  //   programs = await db.program
  //     .find({ $or: [{ type: query.type1 }, { type: query.type2 }], isPublished: true })
  // }
  let count = 0;
  let query = {}
  let query2 = {}

  if (model.ageFrom && model.ageTo) {
    let ageArray = []

    // const agef = {
    //   $gte: Number(model.ageFrom),
    //   $lte: Number(model.ageTo),
    // }
    ageArray.push({ 'ageGroup.from': { '$gte': Number(model.ageFrom), '$lte': Number(model.ageTo) } })
    ageArray.push({ 'ageGroup.to': { '$gte': Number(model.ageFrom), '$lte': Number(model.ageTo) } })
    // const ages = {
    //   $or: ageArray
    // }
    if (query.$and) {
      query.$and.push({
        $or: ageArray
      })
    }
    else {
      query["$and"] = [{
        $or: ageArray
      }]
    }
    // query = ages
    // db.inventory.find({ $or: [{ quantity: { $lt: 20 } }, { price: 10 }] })
    // query["ageGroup.from"] = agef
    // query["ageGroup.from"] = { $gte: Number(model.ageFrom) }
    // query["ageGroup.to"] = { $lte: Number(model.ageTo) }
  }

  if (model.fromDate !== undefined && model.toDate !== undefined && model.fromDate !== "" && model.toDate !== "" && model.fromDate !== null && model.toDate !== null) {
    // query["date.from"] = { $gte: model.fromDate }
    // query["date.to"] = { $lte: model.toDate }
    let start = new Date(model.fromDate)
    let end = new Date(model.toDate)
    let dateArray = []
    dateArray.push({ 'date.from': { '$gte': start, '$lte': end } })
    dateArray.push({ 'date.to': { '$gte': start, '$lte': end } })
    dateArray.push({ 'isDateNotMention': true })
    if (query.$and) {
      query.$and.push({
        $or: dateArray
      })
    }
    else {
      query["$and"] = [{
        $or: dateArray
      }]
    }
  }

  if (model.time !== undefined && model.time !== "" && model.time !== null) {
    //early-morning, morning, afternoon, late-afternoon, evening
    let timeArray = []
    var timeArr = model.time.split(',');
    for (const element of timeArr) {
      if (element == "early-morning") {
        timeArray.push({ 'realTime.from': { '$gte': 6, '$lt': 9 } })
      }
      if (element == "morning") {
        timeArray.push({ 'realTime.from': { '$gte': 9, '$lt': 12 } })
      }
      if (element == "afternoon") {
        timeArray.push({ 'realTime.from': { '$gte': 12, '$lt': 15 } })
      }
      if (element == "late-afternoon") {
        timeArray.push({ "realTime.from": { '$gte': 15, '$lt': 18, } })
      }
      if (element == "evening") {
        timeArray.push({ "realTime.from": { '$gte': 18, '$lt': 21, } })
      }

    }
    if (query.$and) {
      query.$and.push({
        $or: timeArray
      })
    }
    else {
      query["$and"] = [{
        $or: timeArray
      }]
    }
  }
  if (model.toTime !== undefined && model.fromTime !== undefined && model.toTime !== "" && model.fromTime !== "" && model.toTime !== null && model.fromTime !== null) {
    const tme = {
      $gte: model.fromTime,
      $lt: model.toTime,
    }

    query["realTime.from"] = tme
  }
  if (model.priceFrom !== undefined && model.priceTo !== undefined && model.priceFrom !== "" && model.priceTo !== "" && model.priceFrom !== null && model.priceTo !== null) {
    const byPrice = {
      $gte: Number(model.priceFrom),
      $lte: Number(model.priceTo),
    }
    query["pricePerParticipant"] = byPrice
  }
  if (model.durationMin !== undefined && model.durationMax !== undefined && model.durationMin !== "" && model.durationMax !== "" && model.durationMin !== null && model.durationMax !== null) {
    const byduration = {
      $gte: model.durationMin,
      $lte: model.durationMax,
    }
    query["duration.hours"] = byduration
  }
  if (model.ratingFrom !== undefined && model.ratingTo !== undefined && model.ratingFrom !== "" && model.ratingTo !== "" && model.ratingFrom !== null && model.ratingTo !== null) {
    const byRating = {
      $gte: Number(model.ratingFrom),
      $lte: Number(model.ratingTo),
    }
    query2["user.averageFinalRating"] = byRating
  }

  if (model.categoryId !== undefined && model.categoryId !== "" && model.categoryId !== null) {
    // query["categoryId"] = ObjectId(model.categoryId);
    const categorysArray = []
    var categoryArr = model.categoryId.split(',');
    for (const element of categoryArr) {
      categorysArray.push(ObjectId(element))
    }
    query["categoryId"] = { $in: categorysArray }
  }
  if (model.type !== undefined && model.type !== "" && model.type !== null) {
    // query["type"] = model.type;
    const typeArray = []
    var typeArr = model.type.split(',');
    for (const element of typeArr) {
      typeArray.push(element)
    }
    // const types = { type: { $in: typeArray } }
    query["type"] = { $in: typeArray }
  }
  //Drops-in,Semesters,Camps,Other
  if (model.inpersonOrVirtual == 'inperson') {
    query["inpersonOrVirtual"] = 'Inperson'
  }
  if (model.inpersonOrVirtual == 'online') {
    query["inpersonOrVirtual"] = 'Virtual'
  }
  if (model.day !== undefined && model.day !== "" && model.day !== null) {
    console.log('day =>', model.day);
    const dayArray = []
    var nameArr = model.day.split(',');
    for (const element of nameArr) {
      if (element == "sunday") { dayArray.push({ "days.sunday": true }) }
      if (element == "monday") { dayArray.push({ "days.monday": true }) }
      if (element == "tuesday") { dayArray.push({ "days.tuesday": true }) }
      if (element == "wednesday") { dayArray.push({ "days.wednesday": true }) }
      if (element == "thursday") { dayArray.push({ "days.thursday": true }) }
      if (element == "friday") { dayArray.push({ "days.friday": true }) }
      if (element == "saturday") { dayArray.push({ "days.saturday": true }) }
    }
    if (query.$and) {
      query.$and.push({
        $or: dayArray
      })
    }
    else {
      query["$and"] = [{
        $or: dayArray
      }]
    }
  }
  if (model.tagsIds !== undefined && model.tagsIds !== "" && model.tagsIds !== null) {
    const tagsArray = []
    var tagArr = model.tagsIds.split(',');
    for (const element of tagArr) {
      tagsArray.push(ObjectId(element))
      // count += await db.program.find({ subCategoryIds: ObjectId(element), isPublished: true, isExpired: false }).count()
    }
    // const tags = { subCategoryIds: { $in: tagsArray } }
    query["subCategoryIds"] = { $in: tagsArray }


  }
  if (model.lat !== undefined && model.lng !== undefined && model.lat !== "" && model.lng !== "" && model.lat !== null && model.lng !== null) {
    var lt = model.lat.substring(0, 3)
    var lg = model.lng.substring(0, 3)
    const nearbyArray = []
    nearbyArray.push({
      lat: {
        $regex: lt,
        $options: 'i'
      }
    })
    nearbyArray.push({
      lng: {
        $regex: lg,
        $options: 'i'
      }
    })
    const closeBy = {
      $and: nearbyArray
    }
    query = closeBy
  }

  // if (model.ageFrom || model.fromDate || model.toTime || model.priceFrom || model.durationMin || model.categoryId || model.type1 || model.type2) {
  //   query["isPublished"] = true
  // }
  if (model.isChildCare !== undefined && model.isChildCare !== "" && model.isChildCare !== null) {
    let child = JSON.parse(model.isChildCare);
    if (child == false && query.$and) {
      let childArry = [{ isChildCare: null }, { isChildCare: child, }]
      query.$and.push({
        $or: childArry
      })
    }
    else if (child == false && !query.$and) {
      query['$and'] = [{
        $or: [{ isChildCare: null }, { isChildCare: child, }]
      }]
    }
    else {
      query["isChildCare"] = child
    }
  }
  if (model.isFree !== undefined && model.isFree !== "" && model.isFree !== null) {
    let child = JSON.parse(model.isFree);
    if (child == false && query.$or) {
      let childArry = [{ isFree: null }, { isFree: child, }]
      for (let item of childArry) {
        query.$or.push(item)
      }
    }
    else if (child == false && !query.$or) {
      query['$or'] = [{ isFree: null }, { isFree: child, }]
    }
    else {
      query["isFree"] = child
    }
  }
  if (model.indoorOroutdoor !== undefined && model.indoorOroutdoor !== "" && model.indoorOroutdoor !== null) {
    query["indoorOroutdoor"] = { $regex: '.*' + model.indoorOroutdoor + '.*', $options: 'i' }
  }
  if (model.location !== undefined && model.location !== "" && model.location !== null) {
    query["location"] = { $regex: '.*' + model.location + '.*', $options: 'i' }
  }
  query["isPublished"] = true
  query["isExpired"] = false
  const isEmpty = Object.keys(query).length === 0
  if (model.providerId) {
    query["user"] = ObjectId(model.providerId)
  }
  let programs
  if (!isEmpty) {
    // programs = await db.program.find(query)
    //   .sort({ programRating: -1 })
    //   .populate('tags')
    //   .populate('categoryId')
    //   .populate('subCategoryIds')
    //   .populate('user')
    //   .skip(skipCount)
    //   .limit(pageSize)
    //   .skip(skipCount).limit(pageSize);
    // if (query.subCategoryIds) {
    //   let programCount = await db.program.find(query).count();
    //   console.log('count', programCount)
    // }

    count = await db.program.find(query).count()
    console.log("query", query, count)
    programs = await db.program.aggregate([
      {
        $match: query
      },
      { $sort: { user: 1 } },
      {
        $group:
        {
          _id: "$user",
          programs: { $push: "$$ROOT" },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'programs.user',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $lookup: {
          from: 'categories',
          localField: 'programs.categoryId',
          foreignField: '_id',
          as: 'categories',
        },
      },
      { $unwind: "$user" },
      {
        $match: {
          "user.isActivated": true
        }
      },
      {
        $match: query2
      },
      { $sort: { "user.averageFinalRating":-1} },
      { $limit: pageSize + skipCount },
      { $skip: skipCount },
    ])
    log.end()

  }
  const filterprograms = await db.program.aggregate([
    {
      $match: query
    },
    {
      $group:
      {
        _id: "$user",
        total: { $sum: 1 },
        programs: { $push: "$$ROOT" },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'programs.user',
        foreignField: '_id',
        as: 'user',
      },
    },
    { $unwind: "$user" },
    {
      $match: {
        "user.isActivated": true
      }
    },
    {
      $match: query2
    },
    {
    $sort:{"user.averageFinalRating":-1}
    },
    { $count: "totalCount" }
  ])
  programs.count = count
  programs.map(async (program) => {
    var result = program.programs.reverse().filter( function (a) {
      var key = a.ageGroup.from + '|' + a.ageGroup.to + '|' + a.type;
      console.log("key",key)
      if (!this[key]) {
        this[key] = true;
        return true;
      }
    }, Object.create(null))
    program.programs = result
    if (context.user !== undefined) {
      const Favourites = await db.saveProvider.findOne({ $and: [{ parent: context.user.id }, { provider: program._id }] })
      if (Favourites) {
        program.user.isFav = true
      }
    }
    program.programs.map(async(a)=>{
      var isfav=await checkFav(a,context)
      if(isfav==true){
       a.isFav=true
      }
      return a
    })
    let locations= await db.providerlocation.aggregate([
      {
          $match: {
              user: program._id,
          },
      },
  ])
  program.multiLoctions=locations
    return program.programs.sort((a, b) => new Date(b.date.from).valueOf() - new Date(a.date.from).valueOf());
  })

  // programs.map((program) => {
  //   var result = program.programs.reverse().filter(async function (a) {
  //     var key = a.ageGroup.from + '|' + a.ageGroup.to + '|' + a.type;
  //     if (!this[key]) {
  //       let favourites
  //       if (context.user !== undefined) {
  //         favourites = await db.favourite
  //           .findOne({ user: context.user.id, program: a._id })
  //       }
  //       if (favourites) {
  //         a.isFav = true
  //       }

  //       this[key] = true;
  //       return true;
  //     }
  //   }, Object.create(null));
  //   console.log("7676493839483434834", result)

  //   program.programs = result
  //   return program.programs.sort((a, b) => new Date(b.date.from).valueOf() - new Date(a.date.from).valueOf());
  // })
  programs.programCount
  programs.sort((a, b) => b.user.averageFinalRating - a.user.averageFinalRating)
  // if (!isEmpty) {
  //   programs = await db.program.find(query)
  // }
  const allprograms = await db.program.aggregate([
    {
      $match: {
        isPublished: true,
        isExpired: false
      },
    },
    {
      $group:
      {
        _id: "$user",
        total: { $sum: 1 },
      },
    },
    { $count: "totalCount" }
  ])

  let calculationPrograms = {

    totalPrograms: await db.program.find({ isExpired: false }).count(),
    publishPrograms: await db.program.find({ isPublished: true, isExpired: false }).count(),
    unpublishPrograms: await db.program.find({ isPublished: false, isExpired: false }).count(),
    totalProviders: allprograms.length > 0 ? allprograms[0].totalCount : 0,
    filterCount: count,
    filterProviders: filterprograms.length > 0 ? filterprograms[0].totalCount : 0

    //   allPrograms: await db.program.find({}).count(),
    //  ActivePrograms: await db.program.find({isExpired:false}).count(),
    //  expiredPrograms: await db.program.find({isExpired:true}).count(),
    //  publishActivePrograms:await db.program.find({isPublished:true,isExpired:false}).count(),
    //  unpublishActivePrograms:await db.program.find({isPublished:false,isExpired:false}).count(),
    //  totalProviders:allprograms[0].totalCount
  }
  programs.calculationPrograms = calculationPrograms;

  

  return programs
}

// programs list near by location
const nearBy = async (query, context) => {
  const log = context.logger.start(`services:programs:nearBy`)
  const { lat, lng } = query;
  var lt = lat.substring(0, 3)
  var lg = lng.substring(0, 3)

  const programs = await db.program.aggregate([
    {
      $match: {
        $and: [{
          lat: {
            $regex: lt,
            $options: 'i'
          }
        }, {
          lng: {
            $regex: lg,
            $options: 'i'
          }
        }
        ]
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'categoryId',
        foreignField: '_id',
        as: 'categoryId',
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'provider',
      },
    },
  ]).limit(10)
  log.end()
  return programs
}


const subCategoryFilter = async (model, context) => {
  const log = context.logger.start(`services:programs:subCategoryFilter`)
  let pageNo = Number(model.pageNo) || 1
  let pageSize = Number(model.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let filter = []
  if (model.subId1 !== undefined && model.subId1 !== "" && model.subId1 !== null) {
    filter.push(model.subId1);
  }
  if (model.subId2 !== undefined && model.subId2 !== "" && model.subId2 !== null) {
    filter.push(model.subId2);
  }
  if (model.subId3 !== undefined && model.subId3 !== "" && model.subId3 !== null) {
    filter.push(model.subId3);
  }
  if (model.subId4 !== undefined && model.subId4 !== "" && model.subId4 !== null) {
    filter.push(model.subId4);
  }
  if (model.subId5 !== undefined && model.subId5 !== "" && model.subId5 !== null) {
    filter.push(model.subId5);
  }
  const programs = await db.program.find({ subCategoryIds: { $in: filter }, })
    .populate('tags')
    .populate('user')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
    .skip(skipCount)
  log.end();
  return programs
}
// const programs = await db.program.find({ subCategoryIds: { $in: filter }, }).skip(skipCount)
//==-----------------------------------------------------------
const addExcelPrograms = async (model, context, categoriesIds, subcategoriesIds, age, days, provider) => {
  // const addExcelPrograms = async (model, context, sourcs, sourcsUrl, age) => {
  const log = context.logger.start(`services:programs:build${model}`)
  let word
  if (model.name) {
    word = humanize(model.name);
  }
  let isPublished = false
  let fromT = model.startTime.replace(/:/g, '.');
  let tTime = model.endTime.replace(/:/g, '.');

  let realTime = {}
  realTime.from = Number(fromT.slice(0, -3))
  realTime.to = Number(tTime.slice(0, -3))

  let date = {}
  date.from = model.startDate;
  date.to = model.endDate;

  const program = await new db.program({
    name: model.name,
    description: model.description,
    providerName: model.providerName,
    indoorOroutdoor: model.indoorOroutdoor,
    inpersonOrVirtual: model.inpersonOrVirtual,
    source: model.source,
    sourceUrl: model.sourceUrl,
    city: model.city,
    cycle: model.cycle,
    activeStatus: model.activeStatus,
    alias: word ? word : '',

    type: model.type,
    price: model.price,
    priceUnit: model.priceUnit,
    pricePerParticipant: model.price,
    pricePeriod: model.pricePeriod,
    code: model.code,
    lat: model.lat,
    lng: model.lng,
    programCoverPic: model.programCoverPic,
    location: model.location,
    ageGroup: age,
    date: date,
    time: realTime,
    realTime: realTime,
    bookingCancelledIn: model.bookingCancelledIn,
    duration: model.duration,
    isFree: model.isFree,
    priceForSiblings: model.priceForSiblings,
    specialInstructions: model.specialInstructions,
    adultAssistanceIsRequried: model.adultAssistanceIsRequried,
    capacity: model.capacity,
    joiningLink: model.joiningLink,
    presenter: model.presenter,
    emails: model.email,
    batches: model.batches,
    programImage: model.programImage,
    isPublished,
    inpersonOrVirtual: model.inpersonOrVirtual,
    days: days,
    specialInstructions: model.specialInstructions,
    parentRequired: model.parentRequired,
    // dbStatus: model.dbStatus,
    sessionLength: model.sessionLength,

    status: 'active',
    user: provider,
    addresses: model.addresses,
    categoryId: categoriesIds,
    subCategoryIds: subcategoriesIds,
    // categoryId: model.categoryId,
    // subCategoryIds: model.subCategoryIds,
    source: model.source,
    sourceUrl: model.sourceUrl,
    sessions: model.sessions,
    extractionDate: model.extractionDate,
    extractor_notes: model.extractor_notes,
    proofreaderObservation: model.proofreaderObservation,
    extractionComment: model.extractionComment,
    cyrilComment: model.cyrilComment,
    cyrilApproval: model.cyrilApproval,
    proofreaderRating: model.proofreaderRating,
    createdOn: new Date(),
    updateOn: new Date(),
  }).save()
  log.end()
  return program
}


async function getDays(str) {
  let arr = {}
  var str_array = str.split(',');
  console.log('str_array', str_array)
  for (var i = 0; i < str_array.length; i++) {
    str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
    if (str_array[i] == "monday") { arr.monday = true }
    if (str_array[i] == "tuesday") { arr.tuesday = true }
    if (str_array[i] == "wednesday") { arr.wednesday = true }
    if (str_array[i] == "thursday") { arr.thursday = true }
    if (str_array[i] == "friday") { arr.friday = true }
    if (str_array[i] == "saturday") { arr.saturday = true }
    if (str_array[i] == "sunday") { arr.sunday = true }
  }
  return arr
}
async function getIds(record) {
  let data = []
  let categoryArr = []
  if (record.subCategory1) {
    let tag = await db.tag.findOne({ name: { $eq: record.subCategory1 } })
    data.push(tag._id);
  }
  if (record.subCategory2) {
    let tag = await db.tag.findOne({ name: { $eq: record.subCategory2 } })
    data.push(tag._id);
  }
  if (record.subCategory3) {
    let tag = await db.tag.findOne({ name: { $eq: record.subCategory3 } })
    data.push(tag._id);
  }
  if (record.subCategory4) {
    let tag = await db.tag.findOne({ name: { $eq: record.subCategory4 } })
    data.push(tag._id);
  }
  if (record.subCategory5) {
    let tag = await db.tag.findOne({ name: { $eq: record.subCategory5 } })
    data.push(tag._id);
  }

  return data
}

async function categoryIds(record) {
  let data = []
  if (record.category1) {
    let category = await db.category.findOne({ name: { $eq: record.category1 } })
    data.push(category._id);
  }
  if (record.category2) {
    let category = await db.category.findOne({ name: { $eq: record.category2 } })
    data.push(category._id);
  }
  return data
}

async function getAge(str) {
  let ageGroup = {}
  var str_array = str.split('-');
  for (var i = 0; i < str_array.length; i++) {
    str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
    ageGroup.from = str_array[0]
    ageGroup.to = str_array[1]
  }
  return ageGroup
}

const uploadExcel = async (file, context) => {
  const log = context.logger.start(`services:programs:uploadExcel`);
  if (!file) {
    throw new Error("file not found");
  }
  xlsxtojson({
    input: file.path,  // input xls
    output: "output.json", // output json 
    lowerCaseHeaders: true
  }, async function (err, result) {
    if (err) {
      console.log('error in xlsx ==>>>>', err);
    }
    if (result) {
      let categries = []
      let subcategries = []
      let sourcs = []
      let sourcsUrl = []
      let age = []
      let days = {};
      let count = 0;
      result.forEach(async function (record) {
        categries = await categoryIds(record);
        subcategries = await getIds(record);
        days = await getDays(record.days);
        // console.log('days', days);
        const provider = await db.user.findOne({ firstName: { $eq: record.providerName } });

        // sourcs = await getSources(record.source, 'source');
        // sourcsUrl = await getSourcesUrl(record.sourceUrl, 'sourceUrl');
        age = await getAge(record.ageGroup)
        addExcelPrograms(record, context, categries, subcategries, age, days, provider._id)
        // addExcelPrograms(record, context, sourcs, sourcsUrl, age)
      });
    }
  });
  log.end();
  await fs.unlinkSync(file.path)
  return "excel file uploaded successfully"
};

const duplicateCreate = async (id, context) => {
  const log = context.logger.start('services:programs:duplicateCreate')
  let progrm = await db.program.findById(id)

  if (!progrm) {
    throw new Error('original program is not found')
  }

  const program = build(progrm, context)
  log.end()
  return program
}

const childTagProgramCount = async (model, context) => {
  const log = context.logger.start(`services:programs:childTagProgramCount`);
  const age = {
    $gte: 0,
    $lte: Number(model.maxAge),
  }
  const count = await db.program.find({
    $and: [{ subCategoryIds: model.tagId }, { "ageGroup.to": age }],
  }).count()

  log.end()
  return count
}

const expireProgram = async (model, context) => {
  const log = context.logger.start(`services:programs:expireProgram`);
  const program = await db.program.findById(model.id);
  if (!program) {
    throw new Error('program does not exist');
  }
  program.isExpired = true;
  program.expireReason = model.reason;

  log.end()
  await program.save();
  return program
}

const expiresInWeek = async (query, context) => {
  const log = context.logger.start(`services:programs:expiresInWeek`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  const currentDate = new Date();
  let week = moment(currentDate).add(7, 'd')
  let programs = await db.program
    .find({ 'date.to': { '$gte': currentDate, '$lte': week._d } })
    .populate('tags')
    .populate('user')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
    .skip(skipCount)
    .limit(pageSize)
  programs.count = await db.program.find().count()
  log.end()
  return programs
}

const searchByKeyValue = async (query, context) => {
  const log = context.logger.start(`services:programs:searchByKeyValue`)
  // keyType, keyValue
  let program
  if (query.keyType == "name") {
    program = await db.program
      .find({ name: { $regex: '.*' + query.keyValue + '.*', $options: 'i' } })
      .populate('user').populate('tags').populate('subCategoryIds').limit(5)
  }
  if (query.keyType == "type") {
    program = await db.program
      .find({ type: { $regex: '.*' + query.keyValue + '.*', $options: 'i' } })
      .populate('user').populate('tags').populate('subCategoryIds').limit(5)
  }
  if (query.keyType == "address") {
    program = await db.program
      .find({ addresses: { $regex: '.*' + query.keyValue + '.*', $options: 'i' } })
      .populate('user').populate('tags').populate('subCategoryIds').limit(5)
  }
  if (query.keyType == "location") {
    program = await db.program
      .find({ location: { $regex: '.*' + query.keyValue + '.*', $options: 'i' } })
      .populate('user').populate('tags').populate('subCategoryIds').limit(5)
  }

  log.end()
  return program
}

const getExpiredprograms = async (query, context) => {
  const log = context.logger.start(`services:programs:getExpiredprograms`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs = await db.program
    .find({ isExpired: true })
    .sort({ _id: -1 })
    .populate('tags')
    .populate('user')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
    .skip(skipCount)
    .limit(pageSize)
  programs.count = await db.program.find({ isExpired: true }).count()
  let favourites
  if (context.user !== undefined) {
    favourites = await db.favourite
      .find({ user: context.user.id })
      .populate('program')
  }
  if (favourites) {
    // add fav in program
    for (var p = 0; p < programs.length; p++) {
      for (var f = 0; f < favourites.length; f++) {
        if (
          favourites[f].program !== null &&
          favourites[f].program !== undefined
        ) {
          if (programs[p].id === favourites[f].program.id) {
            programs[p].isFav = true
          }
        }
      }
    }
  }
  log.end()
  return programs
}

const montclairPrograms = async (query, context) => {
  const log = context.logger.start(`services:programs:montclairPrograms`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs = await db.program
    .find({ location: { $regex: '.*' + 'montclair' + '.*', $options: 'i' } })
    .sort({ _id: -1 })
    .populate('tags')
    .populate('user')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
    .skip(skipCount)
    .limit(pageSize)
  programs.count = await db.program.find({ location: { $regex: '.*' + 'montclair' + '.*', $options: 'i' } }).count()
  log.end()
  return programs
}
// var startMonth = moment(Month).startOf('month').format('YYYY-MM-DD');
// var endMonth = moment(Month).endOf('month').format('YYYY-MM-DD');
const histogram = async (query, context) => {
  const log = context.logger.start(`services:programs:getProgramCount`)
  if (query.period == 'lastweek') {
    var dateformat = "YYYY/MM/DD";
    let result = []
    let day = moment().subtract(6, 'days')
    for (var i = 0; i < 7; i++) {
      const momentDate = moment.utc(day)
      const startOfDay = momentDate.clone().startOf("day")
      const endOfDay = momentDate.clone().endOf("day")
      let compute
      let computeUpdated


      let existingPrograms
      let existingProvidersActivities = 0
      let newProvidersActivities = 0
      let newProvider = await db.program.aggregate([
        {
          $match: {
            createdOn: { $gte: new Date(startOfDay), $lt: new Date(endOfDay), },
          },
        },
        {
          $lookup: {
            from: 'users',
            localField: 'user',
            foreignField: '_id',
            as: 'provider',
          },
        },
        { $unwind: "$provider" },
        {
          $match: {
            "provider.createdOn": {
              $gte: new Date(startOfDay), $lt: new Date(endOfDay),
            },
            "provider.role": "provider"
          },

        },
        { $count: "totalCount" }

      ]
      )

      if (newProvider.length) {
        newProvidersActivities = newProvider[0].totalCount
      }
      let existingProvider = await db.program.aggregate([
        {
          $match: {
            createdOn: { $gte: new Date(startOfDay), $lt: new Date(endOfDay), },
          },
        },
        {
          $lookup: {
            from: 'users',
            localField: 'user',
            foreignField: '_id',
            as: 'provider',
          },
        },
        { $unwind: "$provider" },
        {
          $match: {
            "provider.createdOn": {
              $lt: new Date(startOfDay),
            },
            "provider.role": "provider"
          },

        },
        { $count: "totalCount" }

      ]
      )

      if (existingProvider.length) {
        existingProvidersActivities = existingProvider[0].totalCount
        console.log(existingProvidersActivities, "existingProvidersActivitiesexistingProvidersActivities")
      }
      compute = await db.program.find({ createdOn: { $gte: startOfDay, $lt: endOfDay, } }).count()
      computeUpdated = await db.program.find({ updatedOn: { $gte: startOfDay, $lt: endOfDay, } }).count()
      existingPrograms = await db.program.find({ createdOn: { $lt: startOfDay, } }).count()
      var weekDayName = moment(day.format(dateformat)).format('dddd');
      result.push({
        newProgram: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
        existingPrograms: existingPrograms, date: endOfDay.format(dateformat), dayName: weekDayName
      });
      day.add(1, "day")
    }
    log.end()
    return result.sort((a, b) => b.date - a.date);
  }
  if (query.period == 'week') {
    let data = [];
    let Month = moment(new Date()).format('YYYY-MM-DD');
    let seven = moment(Month).subtract(7, 'days').format('YYYY-MM-DD')
    let six = moment(Month).subtract(14, 'days').format('YYYY-MM-DD')
    let fifth = moment(Month).subtract(21, 'days').format('YYYY-MM-DD')
    let fourth = moment(Month).subtract(28, 'days').format('YYYY-MM-DD')
    let third = moment(Month).subtract(35, 'days').format('YYYY-MM-DD')
    let second = moment(Month).subtract(42, 'days').format('YYYY-MM-DD')
    let first = moment(Month).subtract(49, 'days').format('YYYY-MM-DD')
    let compute
    let computeUpdated
    let existingPrograms
    let existingProvidersActivities = 0
    let newProvidersActivities = 0
    compute = await db.program.find({ createdOn: { $gte: first, $lt: second, } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: first, $lt: second, } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: first, } }).count()
    let newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(first), $lt: new Date(second), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(first), $lt: new Date(second),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    let existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(first), $lt: new Date(second), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(first),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      week: 1, count: compute, updatedCount: computeUpdated, start: first, end: second, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
      existingPrograms: existingPrograms,
    })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: second, $lt: third, } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: second, $lt: third, } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: second, } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(second), $lt: new Date(third), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(second), $lt: new Date(third),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(second), $lt: new Date(third), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(second),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      week: 2, count: compute, updatedCount: computeUpdated, start: second, end: third, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
      existingPrograms: existingPrograms,
    })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: third, $lt: fourth, } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: third, $lt: fourth, } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: third, } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(third), $lt: new Date(fourth), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(third), $lt: new Date(fourth),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(third), $lt: new Date(fourth), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(third),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      week: 3, count: compute, updatedCount: computeUpdated, start: third, end: fourth, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
      existingPrograms: existingPrograms,
    })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: fourth, $lt: fifth, } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: fourth, $lt: fifth, } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: fourth, } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(fourth), $lt: new Date(fifth), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(fourth), $lt: new Date(fifth),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(fourth), $lt: new Date(fifth), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(fourth),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      week: 4, count: compute, updatedCount: computeUpdated, start: fourth, end: fifth, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
      existingPrograms: existingPrograms,
    })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: fifth, $lt: six, } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: fifth, $lt: six, } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: fifth, } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(fifth), $lt: new Date(six), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(fifth), $lt: new Date(six),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(fifth), $lt: new Date(six), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(fifth),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      week: 5, count: compute, updatedCount: computeUpdated, start: fifth, end: six, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
      existingPrograms: existingPrograms,
    })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: six, $lt: seven, } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: six, $lt: seven, } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: six, } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(six), $lt: new Date(seven), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(six), $lt: new Date(seven),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(six), $lt: new Date(seven), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(six),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      week: 6, count: compute, updatedCount: computeUpdated, start: six, end: seven, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
      existingPrograms: existingPrograms,
    })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: seven, $lt: Month, } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: seven, $lt: Month, } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: seven, } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(seven), $lt: new Date(Month), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(seven), $lt: new Date(Month),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(seven), $lt: new Date(Month), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(seven),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      week: 7, count: compute, updatedCount: computeUpdated, start: seven, end: Month, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities,
      existingPrograms: existingPrograms,
    })
    log.end()
    return data
  }
  if (query.period == 'month') {
    let data = [];
    let seven = moment(new Date()).format('YYYY-MM-DD')
    let six = moment(new Date()).subtract(1, 'M').format('YYYY-MM-DD')
    let fifth = moment(new Date()).subtract(2, 'M').format('YYYY-MM-DD')
    let fourth = moment(new Date()).subtract(3, 'M').format('YYYY-MM-DD')
    let third = moment(new Date()).subtract(4, 'M').format('YYYY-MM-DD')
    let second = moment(new Date()).subtract(5, 'M').format('YYYY-MM-DD')
    let first = moment(new Date()).subtract(6, 'M').format('YYYY-MM-DD')
    let compute
    let computeUpdated
    let existingProvider
    let newProvider
    let existingPrograms
    let existingProvidersActivities = 0
    let newProvidersActivities = 0
    compute = await db.program.find({ createdOn: { $gte: moment(first).startOf('month').format('YYYY-MM-DD'), $lt: moment(first).endOf('month').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(first).startOf('month').format('YYYY-MM-DD'), $lt: moment(first).endOf('month').format('YYYY-MM-DD'), } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: moment(first).startOf('month').format('YYYY-MM-DD'), } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(first).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(first).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(first).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(first).endOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(first).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(first).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(first).startOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({
      month: 1, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(first).startOf('month').format('YYYY-MM-DD')
    })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(second).startOf('month').format('YYYY-MM-DD'), $lt: moment(second).endOf('month').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(second).startOf('month').format('YYYY-MM-DD'), $lt: moment(second).endOf('month').format('YYYY-MM-DD'), } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: moment(second).startOf('month').format('YYYY-MM-DD'), } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(second).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(second).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(second).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(second).endOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(second).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(second).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(second).startOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ month: 2, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(second).startOf('month').format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(third).startOf('month').format('YYYY-MM-DD'), $lt: moment(third).endOf('month').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(third).startOf('month').format('YYYY-MM-DD'), $lt: moment(third).endOf('month').format('YYYY-MM-DD'), } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: moment(third).startOf('month').format('YYYY-MM-DD'), } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(third).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(third).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(third).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(third).endOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(third).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(third).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(third).startOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ month: 3, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(third).startOf('month').format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(fourth).startOf('month').format('YYYY-MM-DD'), $lt: moment(fourth).endOf('month').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(fourth).startOf('month').format('YYYY-MM-DD'), $lt: moment(fourth).endOf('month').format('YYYY-MM-DD'), } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: moment(fourth).startOf('month').format('YYYY-MM-DD'), } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fourth).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(fourth).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(fourth).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(fourth).endOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fourth).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(fourth).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(fourth).startOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({ month: 4, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(fourth).startOf('month').format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(fifth).startOf('month').format('YYYY-MM-DD'), $lt: moment(fifth).endOf('month').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(fifth).startOf('month').format('YYYY-MM-DD'), $lt: moment(fifth).endOf('month').format('YYYY-MM-DD'), } }).count()
    existingPrograms = await db.program.find({ createdOn: { $lt: moment(fifth).startOf('month').format('YYYY-MM-DD'), } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fifth).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(fifth).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(fifth).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(fifth).endOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fifth).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(fifth).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(fifth).startOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ month: 5, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(fifth).startOf('month').format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(six).startOf('month').format('YYYY-MM-DD'), $lt: moment(six).endOf('month').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(six).startOf('month').format('YYYY-MM-DD'), $lt: moment(six).endOf('month').format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(six).startOf('month').format('YYYY-MM-DD'), } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(six).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(six).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(six).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(six).endOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(six).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(six).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(six).startOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ month: 6, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(six).startOf('month').format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(seven).startOf('month').format('YYYY-MM-DD'), $lt: moment(seven).endOf('month').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(seven).startOf('month').format('YYYY-MM-DD'), $lt: moment(seven).endOf('month').format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(seven).startOf('month').format('YYYY-MM-DD'), } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(seven).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(seven).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(seven).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(seven).endOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(seven).startOf('month').format('YYYY-MM-DD')), $lt: new Date(moment(seven).endOf('month').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(seven).startOf('month').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ month: 7, count: compute, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(seven).startOf('month').format('YYYY-MM-DD') })
    log.end()
    return data
  }
  if (query.period == 'quarter') {
    let data = [];
    let eight = moment(new Date()).format('YYYY-MM-DD')
    let seven = moment(new Date()).subtract(3, 'M').format('YYYY-MM-DD')
    let six = moment(new Date()).subtract(6, 'M').format('YYYY-MM-DD')
    let fifth = moment(new Date()).subtract(9, 'M').format('YYYY-MM-DD')
    let fourth = moment(new Date()).subtract(12, 'M').format('YYYY-MM-DD')
    let third = moment(new Date()).subtract(15, 'M').format('YYYY-MM-DD')
    let second = moment(new Date()).subtract(18, 'M').format('YYYY-MM-DD')
    let first = moment(new Date()).subtract(21, 'M').format('YYYY-MM-DD')
    let compute
    let computeUpdated
    let existingProvider
    let newProvider
    let existingPrograms
    let existingProvidersActivities = 0
    let newProvidersActivities = 0
    compute = await db.program.find({ createdOn: { $gte: moment(first).format('YYYY-MM-DD'), $lt: moment(second).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(first).format('YYYY-MM-DD'), $lt: moment(second).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(first).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(first).format('YYYY-MM-DD')), $lt: new Date(moment(second).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(first).format('YYYY-MM-DD')), $lt: new Date(moment(second).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(first).format('YYYY-MM-DD')), $lt: new Date(moment(second).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(first).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }


    data.push({ Quarter: 1, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(first).format('YYYY-MM-DD'), end: moment(second).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(second).format('YYYY-MM-DD'), $lt: moment(third).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(second).format('YYYY-MM-DD'), $lt: moment(third).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(second).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(second).format('YYYY-MM-DD')), $lt: new Date(moment(third).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(second).format('YYYY-MM-DD')), $lt: new Date(moment(third).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(second).format('YYYY-MM-DD')), $lt: new Date(moment(third).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(second).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({ Quarter: 2, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(second).format('YYYY-MM-DD'), end: moment(third).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(third).format('YYYY-MM-DD'), $lt: moment(fourth).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(third).format('YYYY-MM-DD'), $lt: moment(fourth).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(third).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(third).format('YYYY-MM-DD')), $lt: new Date(moment(fourth).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(third).format('YYYY-MM-DD')), $lt: new Date(moment(fourth).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(third).format('YYYY-MM-DD')), $lt: new Date(moment(fourth).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(third).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ Quarter: 3, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(third).format('YYYY-MM-DD'), end: moment(fourth).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(fourth).format('YYYY-MM-DD'), $lt: moment(fifth).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(fourth).format('YYYY-MM-DD'), $lt: moment(fifth).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(fourth).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fourth).format('YYYY-MM-DD')), $lt: new Date(moment(fifth).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(fourth).format('YYYY-MM-DD')), $lt: new Date(moment(fifth).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fourth).format('YYYY-MM-DD')), $lt: new Date(moment(fifth).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(fourth).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ Quarter: 4, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(fourth).format('YYYY-MM-DD'), end: moment(fifth).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(fifth).format('YYYY-MM-DD'), $lt: moment(six).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(fifth).format('YYYY-MM-DD'), $lt: moment(six).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(fifth).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fifth).format('YYYY-MM-DD')), $lt: new Date(moment(six).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(fifth).format('YYYY-MM-DD')), $lt: new Date(moment(six).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(fifth).format('YYYY-MM-DD')), $lt: new Date(moment(six).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(fifth).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ Quarter: 5, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(fifth).format('YYYY-MM-DD'), end: moment(six).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(six).format('YYYY-MM-DD'), $lt: moment(seven).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(six).format('YYYY-MM-DD'), $lt: moment(seven).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(six).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(six).format('YYYY-MM-DD')), $lt: new Date(moment(seven).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(six).format('YYYY-MM-DD')), $lt: new Date(moment(seven).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(six).format('YYYY-MM-DD')), $lt: new Date(moment(seven).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(six).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({ Quarter: 6, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(six).format('YYYY-MM-DD'), end: moment(seven).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(seven).format('YYYY-MM-DD'), $lt: moment(eight).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(seven).format('YYYY-MM-DD'), $lt: moment(eight).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(seven).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(seven).format('YYYY-MM-DD')), $lt: new Date(moment(eight).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(seven).format('YYYY-MM-DD')), $lt: new Date(moment(eight).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(seven).format('YYYY-MM-DD')), $lt: new Date(moment(eight).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(seven).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ Quarter: 7, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(seven).format('YYYY-MM-DD'), end: moment(eight).format('YYYY-MM-DD') })
    log.end()
    return data

  }
  if (query.period == 'semiYear') {
    let data = [];
    let current = moment(new Date()).format('YYYY-MM-DD')
    let third = moment(new Date()).subtract(6, 'month').format('YYYY-MM-DD')
    let second = moment(new Date()).subtract(12, 'month').format('YYYY-MM-DD')
    let first = moment(new Date()).subtract(18, 'month').format('YYYY-MM-DD')
    let compute
    let computeUpdated
    let existingProvider
    let newProvider
    let existingPrograms
    let existingProvidersActivities = 0
    let newProvidersActivities = 0
    compute = await db.program.find({ createdOn: { $gte: moment(first).format('YYYY-MM-DD'), $lt: moment(second).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(first).format('YYYY-MM-DD'), $lt: moment(second).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(first).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(first).format('YYYY-MM-DD')), $lt: new Date(moment(second).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(first).format('YYYY-MM-DD')), $lt: new Date(moment(second).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(first).format('YYYY-MM-DD')), $lt: new Date(moment(second).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(first).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ semiYear: 1, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(first).format('YYYY-MM-DD'), end: moment(second).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(second).format('YYYY-MM-DD'), $lt: moment(third).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(second).format('YYYY-MM-DD'), $lt: moment(third).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(second).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(second).format('YYYY-MM-DD')), $lt: new Date(moment(third).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(second).format('YYYY-MM-DD')), $lt: new Date(moment(third).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(second).format('YYYY-MM-DD')), $lt: new Date(moment(third).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(second).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }
    data.push({ semiYear: 2, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(second).format('YYYY-MM-DD'), end: moment(third).format('YYYY-MM-DD') })

    existingProvidersActivities = 0
    newProvidersActivities = 0

    compute = await db.program.find({ createdOn: { $gte: moment(third).format('YYYY-MM-DD'), $lt: moment(current).format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(third).format('YYYY-MM-DD'), $lt: moment(current).format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(third).format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(third).format('YYYY-MM-DD')), $lt: new Date(moment(current).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(third).format('YYYY-MM-DD')), $lt: new Date(moment(current).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(third).format('YYYY-MM-DD')), $lt: new Date(moment(current).format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(third).format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ semiYear: 3, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(third).format('YYYY-MM-DD'), end: moment(current).format('YYYY-MM-DD') })
    log.end()
    return data
  }
  if (query.period == 'year') {
    let data = [];
    let current = moment(new Date()).format('YYYY-MM-DD')
    let previous = moment(new Date()).subtract(1, 'year').format('YYYY-MM-DD')
    let compute
    let computeUpdated
    let existingProvider
    let newProvider
    let existingPrograms
    let existingProvidersActivities = 0
    let newProvidersActivities = 0
    compute = await db.program.find({ createdOn: { $gte: moment(previous).startOf('year').format('YYYY-MM-DD'), $lt: moment(previous).endOf('year').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(previous).startOf('year').format('YYYY-MM-DD'), $lt: moment(previous).endOf('year').format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(previous).startOf('year').format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(previous).startOf('year').format('YYYY-MM-DD')), $lt: new Date(moment(previous).endOf('year').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(previous).startOf('year').format('YYYY-MM-DD')), $lt: new Date(moment(previous).endOf('year').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(previous).startOf('year').format('YYYY-MM-DD')), $lt: new Date(moment(previous).endOf('year').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(previous).startOf('year').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ year: 1, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(previous).format('YYYY') })

    existingProvidersActivities = 0
    newProvidersActivities = 0
    compute = await db.program.find({ createdOn: { $gte: moment(current).startOf('year').format('YYYY-MM-DD'), $lt: moment(current).endOf('year').format('YYYY-MM-DD'), } }).count()
    computeUpdated = await db.program.find({ updatedOn: { $gte: moment(current).startOf('year').format('YYYY-MM-DD'), $lt: moment(current).endOf('year').format('YYYY-MM-DD'), } }).count()

    existingPrograms = await db.program.find({ createdOn: { $lt: moment(current).startOf('year').format('YYYY-MM-DD') } }).count()
    newProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(current).startOf('year').format('YYYY-MM-DD'),), $lt: new Date(moment(current).endOf('year').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $gte: new Date(moment(current).startOf('year').format('YYYY-MM-DD'),), $lt: new Date(moment(current).endOf('year').format('YYYY-MM-DD')),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (newProvider.length) {
      newProvidersActivities = newProvider[0].totalCount
    }
    existingProvider = await db.program.aggregate([
      {
        $match: {
          createdOn: { $gte: new Date(moment(current).startOf('year').format('YYYY-MM-DD'),), $lt: new Date(moment(current).endOf('year').format('YYYY-MM-DD')), },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'provider',
        },
      },
      { $unwind: "$provider" },
      {
        $match: {
          "provider.createdOn": {
            $lt: new Date(moment(current).startOf('year').format('YYYY-MM-DD'),),
          },
          "provider.role": "provider"
        },

      },
      { $count: "totalCount" }

    ]
    )

    if (existingProvider.length) {
      existingProvidersActivities = existingProvider[0].totalCount
    }

    data.push({ year: 2, count: compute, updatedCount: computeUpdated, existingProvidersActivities: existingProvidersActivities, newProvidersActivities: newProvidersActivities, existingPrograms: existingPrograms, period: moment(current).format('YYYY') })
    log.end()
    return data
  }
  if (query.fromDate && query.toDate) {
    let data = await db.program.find({ createdOn: { $gte: query.fromDate, $lt: query.toDate, } })
      .populate('tags')
      .populate('user')
      .populate('categoryId')
      .populate('subCategoryIds')
      .populate('lastModifiedBy')
    log.end()
    return data
  }
}

// get categories and subcategories id's function ====
// async function getIds(str, type) {
//   let ids = []
//   var str_array = str.split(',');
//   if (type == 'category') {
//     console.log(' ========categories')
//     for (var i = 0; i < str_array.length; i++) {
//       str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
//       let cate = await db.category.findOne({ name: { $eq: str_array[i] } })
//       if (cate) {
//         ids.push(cate._id)
//       }
//     }
//     return ids;
//   }
//   if (type == 'subcategory') {
//     for (var i = 0; i < str_array.length; i++) {
//       str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
//       let cate = await db.tag.findOne({ name: { $eq: str_array[i] } })
//       if (cate) {
//         ids.push(cate._id)
//       }
//     }
//     return ids;
//   }
// }

// async function getSources(str) {
//   let arr = []
//   var str_array = str.split('and');
//   for (var i = 0; i < str_array.length; i++) {
//     str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
//     arr.push(str_array[i])
//   }
//   return arr
// }

// async function getSourcesUrl(str) {
//   let arr = []
//   var str_array = str.split(';');
//   for (var i = 0; i < str_array.length; i++) {
//     str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
//     arr.push(str_array[i])
//   }
//   return arr
// }
const getProgramsByUser = async (query, context) => {
  const log = context.logger.start(`services:programs:getProgramsByUser`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  if (!query.userId) {
    throw new Error('userId is  required')
  }
  let user = await db.user.findById(query.userId)
  if (!user) {
    throw new Error(
      'provider not found, So without provider programs not possible'
    )
  }
  // let programs = await db.program.find({ user: query.userId }).sort({ createdOn: -1 }).populate('tags').skip(skipCount).limit(pageSize);
  const programs = await db.program.aggregate([
    {
      $match: {
        user: ObjectId(query.userId),
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'categoryId',
        foreignField: '_id',
        as: 'categoryId',
      },
    },
    {
      $lookup: {
        from: 'providers',
        localField: 'user',
        foreignField: 'user',
        as: 'provider',
      },
    },
    {
      $lookup: {
        from: "tags",
        let: { "subCategoryIds": "$subCategoryIds" },
        // pipeline: [
        //   { "$match": { "$expr": { "$in": ["$_id", "$$subCategoryIds"] } } }
        // ],
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $in: ['$_id', { $ifNull: ['$$subCategoryIds', []] }] },
                ]
              }
            }
          }
        ],
        as: "subCategoryIds"
      }
    },
    { $sort: { createdOn: -1 } },
    { $limit: pageSize + skipCount },
    { $skip: skipCount },
  ])
  programs.count = await db.program.find({ user: query.userId }).count()
  log.end()
  let favourites
  if (user) {
    favourites = await db.favourite
      .find({ user: query.userId })
      .populate('program')
  }
  if (favourites) {
    // add fav in program
    for (var p = 0; p < programs.length; p++) {
      for (var f = 0; f < favourites.length; f++) {
        if (
          favourites[f].program !== null &&
          favourites[f].program !== undefined
        ) {
          if (programs[p]._id == favourites[f].program.id) {
            programs[p].isFav = true
          }
        }
      }
    }
  }
  return programs
}
const freeTrail = async (query, context) => {
  const log = context.logger.start(`services:providers:freeTrail`)

  let program = await db.program.findById(query.programId)
  if (context.user.role == 'parent') {
    throw new Error('you are not authorized to perform this operation')
  }
  program.isFreeTrial = query.isFreeTrial
  program.updatedOn = new Date()
  log.end()
  program.save()
  return program
}

const bulkPublishOrUnpublish = async (model, context) => {
  const log = context.logger.start(`services:providers:bulkPublishOrUnpublish`)
  if (context.user.role == 'parent') {
    throw new Error('you are not authorized to perform this operation')
  }
  if (model.programIds.length <= 0) {
    throw new Error('program ids are required to make publish or unpublish')
  }
  if (model.programIds.length > 0) {
    for (let id of model.programIds) {
      let program = await db.program.findById(id)
      program.isPublished = model.isPublished
      program.updatedOn = new Date()
      await program.save()
    }
  }
  log.end()
  if (model.isPublished) { return "published" }
  else { return "unpublished" }
}

const getExpiredByProvider = async (query, context) => {
  const log = context.logger.start(`services:programs:getExpiredByProvider`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  if (!query.userId) {
    throw new Error('userId is  required')
  }
  let user = await db.user.findById(query.userId)
  if (!user) {
    throw new Error(
      'provider not found, So without provider programs not possible'
    )
  }
  // let programs = await db.program.find({ user: query.userId }).sort({ createdOn: -1 }).populate('tags').skip(skipCount).limit(pageSize);
  const programs = await db.program.aggregate([
    {
      $match: {
        user: ObjectId(query.userId),
        isExpired: true
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'categoryId',
        foreignField: '_id',
        as: 'categoryId',
      },
    },
    {
      $lookup: {
        from: 'providers',
        localField: 'user',
        foreignField: 'user',
        as: 'provider',
      },
    },
    { $sort: { createdOn: -1 } },
    { $limit: pageSize + skipCount },
    { $skip: skipCount },
  ])
  programs.count = await db.program.find({ user: query.userId, isExpired: true }).count()
  log.end()
  return programs
}
const getActiveByProvider = async (query, context) => {
  const log = context.logger.start(`services:programs:getActiveByProvider`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  if (!query.userId) {
    throw new Error('userId is  required')
  }
  let user = await db.user.findById(query.userId)
  if (!user) {
    throw new Error(
      'provider not found, So without provider programs not possible'
    )
  }
  // let programs = await db.program.find({ user: query.userId }).sort({ createdOn: -1 }).populate('tags').skip(skipCount).limit(pageSize);
  const programs = await db.program.aggregate([
    {
      $match: {
        user: ObjectId(query.userId),
        isExpired: false
      },
    },
    {
      $lookup: {
        from: 'categories',
        localField: 'categoryId',
        foreignField: '_id',
        as: 'categoryId',
      },
    },
    {
      $lookup: {
        from: 'providers',
        localField: 'user',
        foreignField: 'user',
        as: 'provider',
      },
    },
    { $sort: { createdOn: -1 } },
    { $limit: pageSize + skipCount },
    { $skip: skipCount },
  ])
  programs.count = await db.program.find({ user: query.userId, isExpired: false }).count()
  log.end()
  return programs
}

const bulkExpire = async (model, context) => {
  const log = context.logger.start(`services:providers:bulkExpire`)
  if (context.user.role == 'parent') {
    throw new Error('you are not authorized to perform this operation')
  }
  if (model.programIds.length <= 0) {
    throw new Error('program ids are required in order to expire program')
  }
  if (model.programIds.length > 0) {
    for (let id of model.programIds) {
      let program = await db.program.findById(id)
      program.isPublished = false
      program.isExpired = true
      program.updatedOn = new Date()
      await program.save()
    }
  }
  log.end()
  return "programs expired successfully"
}

const searchWithProviderId = async (query, context) => {
  const log = context.logger.start(`services:programs:searchWithProviderId`)
  // keyType, keyValue
  let program = await db.program
    .find({ name: { $regex: '.*' + query.name + '.*', $options: 'i' }, user: ObjectId(query.providerId) })
    .populate('user').populate('tags').populate('subCategoryIds').limit(20)
  log.end()
  return program
}
const programNotReviewed = async (query, context) => {
  const log = context.logger.start(`services:programs:programNotReviewed`)
  let pageNo = Number(query.pageNo) || 1
  let pageSize = Number(query.pageSize) || 10
  let skipCount = pageSize * (pageNo - 1)
  let programs = await db.program
    .find({ $or: [{ last_reviewed: null }, { last_reviewed: { $lt: "2020-01-01T00:00:00.000Z" }, }] })
    .sort({ _id: -1 })
    .populate('tags')
    .populate('user')
    .populate('categoryId')
    .populate('subCategoryIds')
    .populate('lastModifiedBy')
    .skip(skipCount)
    .limit(pageSize)
  programs.count = await db.program
    .find({ $or: [{ last_reviewed: null }, { last_reviewed: { $lt: "2020-01-01T00:00:00.000Z" }, }] }).count();
  let favourites
  if (context.user !== undefined) {
    favourites = await db.favourite
      .find({ user: context.user.id })
      .populate('program')
  }
  if (favourites) {
    // add fav in program
    for (var p = 0; p < programs.length; p++) {
      for (var f = 0; f < favourites.length; f++) {
        if (
          favourites[f].program !== null &&
          favourites[f].program !== undefined
        ) {
          if (programs[p].id === favourites[f].program.id) {
            programs[p].isFav = true
          }
        }
      }
    }
  }
  log.end()
  return programs
}
const groupBySametypeAndage = async (id, context) => {
  const log = context.logger.start(`services:programs:groupBySametypeAndage`)
  let program = await db.program.findById(id)
  if (!program) {
    throw new Error('program not find')
  }
  let programs = await db.program.aggregate([
    {
      $match: {
        user: ObjectId(program.user),
        "ageGroup.from": program.ageGroup.from,
        "ageGroup.to": program.ageGroup.to,
        type: program.type,
        isExpired: false,
        isPublished: true
      }
    },

    // { "$group": {
    //     "_id":{
    //       "ageFrom":"$ageGroup.from",
    //       "ageTo":"$ageGroup.to",
    //       "type":"$type"
    //     },
    //     programs: { $push: "$$ROOT" },
    // }},
  ])
  log.end()

  return programs
}
exports.create = create
exports.getAllprograms = getAllprograms
exports.update = update
exports.getById = getById
exports.removeById = removeById
exports.uploadTimeLinePics = uploadTimeLinePics
exports.search = search
exports.getProgramsByProvider = getProgramsByProvider
exports.addProgramAction = addProgramAction
exports.getViewCount = getViewCount
exports.getProgramCount = getProgramCount
exports.setActiveOrDecactive = setActiveOrDecactive
exports.getGraphData = getGraphData
exports.getFilterProgram = getFilterProgram
exports.importProgram = importProgram
exports.getProgramsByDate = getProgramsByDate
exports.publishedOrUnPublishedPrograms = publishedOrUnPublishedPrograms
exports.openPrograms = openPrograms
exports.publish = publish
exports.listPublishOrUnpublish = listPublishOrUnpublish
exports.searchByNameAndDate = searchByNameAndDate
exports.uploadExcel = uploadExcel
exports.topRating = topRating
exports.multiFilter = multiFilter
exports.nearBy = nearBy
exports.subCategoryFilter = subCategoryFilter
exports.duplicateCreate = duplicateCreate;
exports.childTagProgramCount = childTagProgramCount;
exports.expireProgram = expireProgram;
exports.expiresInWeek = expiresInWeek;
exports.searchByKeyValue = searchByKeyValue;
exports.getExpiredprograms = getExpiredprograms;
exports.montclairPrograms = montclairPrograms;
exports.histogram = histogram;
exports.groupPublishOrUnpublish = groupPublishOrUnpublish;
exports.freeTrail = freeTrail;
exports.getProgramsByUser = getProgramsByUser;
exports.bulkPublishOrUnpublish = bulkPublishOrUnpublish;
exports.getExpiredByProvider = getExpiredByProvider;
exports.getActiveByProvider = getActiveByProvider;
exports.bulkExpire = bulkExpire;
exports.searchWithProviderId = searchWithProviderId;
exports.programNotReviewed = programNotReviewed;
exports.getByType = getByType;
exports.groupBySametypeAndage = groupBySametypeAndage;
exports.changeProgramDate = changeProgramDate;