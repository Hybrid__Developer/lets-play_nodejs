module.exports = [
    {
        url: "/create",
        post: {
            summary: "create",
            description: "activity post",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of activity creation",
                    required: true,
                    schema: {
                        $ref: "#/definitions/add-activity"
                    }
                },
                // {
                //     in: "header",
                //     name: "x-access-token",
                //     description: "token to access api",
                //     required: true,
                //     type: "string"
                // }
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },

]    