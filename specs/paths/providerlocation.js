module.exports = [
    {
        url: "/add",
        post: {
            summary: "add",
            description: "add in providerLocation",
            parameters: [
                {
                    in: "body",
                    name: "body",
                    description: "Model of favourite creation",
                    required: true,
                    schema: {
                        $ref: "#/definitions/providerLocationCreate"
                    }
                },
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/getAll",
        get: {
            summary: "get providerLocation list",
            description: "get providerLocation list",
            parameters: [

               
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    {
        url: "/getByProviderId/{id}",
        get: {
            url: "/getByParentId",
            summary: "get providerLocation by providerid",
            description: "get providerLocation list by providerid",
            parameters: [
                {
                    in: "path",
                    name: "id",
                    description: "providerId",
                    required: true,
                    type: "string"
                },
            ],
            responses: {
                default: {
                    description: "Unexpected error",
                    schema: {
                        $ref: "#/definitions/Error"
                    }
                }
            }
        }
    },
    
]