module.exports = [

    {
        name: "add-activity",
        properties: {
            programId: {
                type: "string"
            },
            isOpenForBooking: {
                type: "boolean"
            },
            joiningLink: {
                type: "string"
            },
            date: {
                type: "date"
            },
            isActivityRecurring: {
                type: "boolean"
            },
            extractionDate: {
                type: "date"
            }, 
            totalSessionClasses: {
                type: "number"
            },
            registrationStartDate: {
                type: "date"
            },
            registrationEndDate: {
                type: "date"
            },
            price: {
                type: "number"
            },
            priceUnit: {
                type: "string"
            }, 
            isPriceNotMention: {
                type: "boolean"
            },
            isPriceProrated: {
                type: "boolean"
            },
            offerDiscount: {
                type: "string"
            },
            isFreeTrial: {
                type: "boolean"
            },
            specialInstructions: { 
                type: "string"  
            },
            cancelBeforeHours: { 
                type: "number"  
            },
            cancelBeforeDays: { 
                type: "number"  
            },
            extractor_notes: { 
                type: "string"  
            },
            proofreaderLastReview: { 
                type: "date"  
            },
            time: {
                properties: {
                  from: { type: 'number' },
                  to: { type: 'number' },
                },
              },
              days: {
                properties: {
                  sunday: { type: 'boolean' },
                  monday: { type: 'boolean' },
                  tuesday: { type: 'boolean' },
                  wednesday: { type: 'boolean' },
                  thursday: { type: 'boolean' },
                  friday: { type: 'boolean' },
                  saturday: { type: 'boolean' },
                },
              },
              duration: {
                properties: {
                  minutes: { type: 'number' },
                  hours: { type: 'number' },
                },
              },

        }
    }
];

 