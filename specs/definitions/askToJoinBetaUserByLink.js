
module.exports = [
    {
        name: "askToJoinBetaUserByLink",
        properties: {
            firstName: {
                type: "string"
            },
            lastName: {
                type: "string"
            },
            userName: {
                type: "string"
            },
            location: {
                type: "string"
            },
            addressLine1: {
                type: "string"
            },
            addressLine2: {
                type: "string"
            },
            // lookingkidsActivityIn: {
            //     type: "string"
            // },
            lat: {
                type: "string"
            },
            lng: {
                type: "string"
            },
            // bookedActivityInLastMonths: {
            //     type: "string"
            // },
            // wantWondrflyBetaUserBecause: {
            //     type: "string"
            // },
            // occupation: {
            //     type: "string"
            // },
            // willActive: {
            //     type: "boolean"
            // },
            ipAddress: {
                type: "string"
            },
            // role: {
            //     type: "string"
            // }
        }
    }
];



