module.exports = [
    {
        name: 'providerLocationCreate',
        properties: {
            name: {
                type: 'string',
            },
            user: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            lat: {
                type: 'integer',
            },
            lng: {
                type: 'integer',
            },
        }
    }
]